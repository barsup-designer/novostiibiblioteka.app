namespace Bars.ModulNews
{
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Extensions.Expressions;
    using BarsUp.Modules.PostgreSql.DataAccess.Npgsql;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using NHibernate.Hql.Ast;
    using NHibernate.Linq.Functions;
    using NHibernate.Linq.Visitors;
    using NHibernate.Linq;
    using NHibernate.Util;
    using System.Collections.ObjectModel;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Reflection;
    using System;
    using NHibernate.Cfg;

    /// <summary>
    /// Конфигуратор NHibernate
    /// </summary>
    internal class NHibernateConfigurator : INhibernateConfigModifier
    {
        public void Apply(Configuration configuration)
        {
        }

        internal static void RegisterAll()
        {
            ExpressionParser.GlobalConfigurators.Add(x => x.Using(typeof(SqlFunctions)));
        }
    }

    /// <summary>
    /// Статические методы для вызова sql-функций в linq-запросах
    /// </summary>
    public static class SqlFunctions
    {
    }

    /// <summary>
    /// Статические методы расширения сущностей для вызова sql-функций в linq-запросах
    /// </summary>
    public static class SqlFunctionsEntitiesExtensions
    {
    }
}