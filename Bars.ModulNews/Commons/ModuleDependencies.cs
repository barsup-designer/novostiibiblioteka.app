namespace Bars.ModulNews
{
    using BarsUp.DataAccess;
    using BarsUp;
    using System.Linq.Expressions;
    using System.Linq;
    using Castle.Windsor;
    using System;

    public class ModuleDependencies : BaseModuleDependencies
    {
        public ModuleDependencies(IWindsorContainer container): base(container)
        {
            RegisterNewsSpisokDependencies();
            RegisterNovostiDependencies();
        }

#region Новости и его наследники
        private void RegisterNewsSpisokDependencies()
        {
#region Новости
            References.Add(new EntityReference{ReferenceName = "Новости прочитанные пользователем", BaseEntity = typeof(Bars.ModulNews.NewsSpisok), CheckAnyDependences = id => Any<Bars.ModulNews.NewsReadLogins>(x => x.NewsSpisok.Id == id), GetCountDependences = id => Count<Bars.ModulNews.NewsReadLogins>(x => x.NewsSpisok.Id == id), GetDescriptionDependences = null});
#endregion
        }

#endregion
#region Новости и его наследники
        private void RegisterNovostiDependencies()
        {
#region Новости
            References.Add(new EntityReference{ReferenceName = "Комментарии", BaseEntity = typeof(Bars.ModulNews.Novosti), CheckAnyDependences = id => Any<Bars.ModulNews.Comment>(x => x.News.Id == id), GetCountDependences = id => Count<Bars.ModulNews.Comment>(x => x.News.Id == id), GetDescriptionDependences = null});
#endregion
        }

#endregion
        private IQueryable<T> GetAll<T>() => Container.Resolve<IDataStore>().GetAll<T>();
        private bool Any<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Any(predicate);
        private int Count<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Count(predicate);
        private void SetNull<T>(Expression<Func<T, bool>> predicate, Action<T> action)
            where T : IEntity
        {
            var ds = Container.Resolve<IDataStore>();
            var records = ds.GetAll<T>().Where(predicate).ToArray();
            foreach (var record in records)
            {
                action.Invoke(record);
                ds.Update(record);
            }
        }

        private void RemoveRefs<T>(Expression<Func<T, bool>> predicate)
            where T : IEntity
        {
            var domainService = Container.ResolveDomain<T>();
            var entities = domainService.GetAll().Where(predicate).Select(x => new
            {
            Type = x.GetType(), Id = x.Id
            }

            ).ToArray().GroupBy(x => x.Type);
            foreach (var type in entities)
            {
                var x = type.Key;
                if (typeof(NHibernate.Proxy.INHibernateProxy).IsAssignableFrom(x))
                    x = type.Key.BaseType;
                var ds = Container.Resolve(typeof(IDomainService<>).MakeGenericType(x)) as IDomainService;
                foreach (var v in type)
                    ds.Delete(v.Id);
            }
        }
    }
}