namespace Bars.ModulNews
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.IoC;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    /// <summary>
    /// Модуль регистрации контроллеров
    /// </summary>    
    public partial class Module
    {
        /// <summary>
        /// Регистрация контроллеров
        /// </summary>
        protected virtual void RegisterControllers()
        {
            Container.RegisterEditorViewServiceController<Comment, CommentEditorModel>("CommentEditor");
            Container.RegisterEditorViewServiceController<NewsSpisok, NewsSpisokFormModel>("NewsSpisokForm");
            Container.RegisterEditorViewServiceController<Novosti, NovostiEditorModel>("NovostiEditor");
            Container.RegisterListViewServiceController<Comment, CommentListModel>("CommentList");
            Container.RegisterListViewServiceController<NewsSpisok, NewsSpisokListModel>("NewsSpisokList");
            Container.RegisterListViewServiceController<Novosti, NovostiListModel>("NovostiList");
        }
    }
}