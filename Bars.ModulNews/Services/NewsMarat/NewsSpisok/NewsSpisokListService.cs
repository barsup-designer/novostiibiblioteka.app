namespace Bars.ModulNews
{
    using Castle.Windsor;
    using System.Diagnostics;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Attributes;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Lists;
    using BarsUp.Designer.GeneratedApp.Queries;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System;
    using NHibernate.Linq;
    using BarsUp.Designer.GeneratedApp;
    using System.Threading.Tasks;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Новости'
    /// </summary>
    public interface INewsSpisokListService : IListViewService<Bars.ModulNews.NewsSpisok, Bars.ModulNews.NewsSpisokListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра для сервиса представления 'Новости'
    /// </summary>
    public interface INewsSpisokListServiceFilter : IListViewServiceFilter<Bars.ModulNews.NewsSpisok, Bars.ModulNews.NewsSpisokListModel, BaseParams>
    {
    }

    /// <summary>
    /// Сервис для представления 'Новости'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Новости")]
    [ApiAccessible("Новости")]
    public class NewsSpisokListService : ListViewService<Bars.ModulNews.NewsSpisok, Bars.ModulNews.NewsSpisokListModel>, INewsSpisokListService
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        public NewsSpisokListService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.ModulNews.NewsSpisok> Filter(IQueryable<Bars.ModulNews.NewsSpisok> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.ModulNews.NewsSpisokListModel> Map(IQueryable<Bars.ModulNews.NewsSpisok> entityQuery)
        {
            var query = DataStore.ReadAll<Bars.ModulNews.NewsSpisok>();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.ModulNews.NewsSpisokListModel{Id = x.Id, _TypeUid = "3795a829-583b-461d-99db-978cb530d0f1", NewsSpisokDate = ((System.DateTime? )(x.NewsSpisokDate)), NewsSpisokVes = ((Bars.ModulNews.NewsPerechVes? )(x.NewsSpisokVes)), NewsSpisokVid = ((Bars.ModulNews.NewsVidPerech? )(x.NewsSpisokVid)), NewsSpisokShortName = ((System.String)(x.NewsSpisokShortName)), });
        }

        protected override async Task ProcessResultAsync(IList<Bars.ModulNews.NewsSpisokListModel> elements, BaseParams @params)
        {
            // устанавливаем контроллер редактора для каждой из сущностей
            foreach (var itm in elements)
            {
                switch (itm._TypeUid)
                {
                    case "3795a829-583b-461d-99db-978cb530d0f1":
                        itm._EditorName = "NewsSpisokForm";
                        break;
                }
            }

            await Task.CompletedTask;
        }
    }
}