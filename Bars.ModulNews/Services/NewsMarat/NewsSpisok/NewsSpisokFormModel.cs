namespace Bars.ModulNews
{
    using BarsUp.Windsor;
    using Newtonsoft.Json;
    using BarsUp.Core.Exceptions;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using BarsUp.Designer.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма для редактирования'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Форма для редактирования")]
    public class NewsSpisokFormModel : DataTransferObject
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public NewsSpisokFormModel()
        {
        }

        /// <summary>
        /// Свойство 'Дата'
        /// </summary>
        [BarsUp.Utils.Display("Дата")]
        [BarsUp.Utils.Attributes.Uid("889f26e4-3fca-4073-8b20-9ba1a0c99c3e")]
        [JsonConverter(typeof(DateTimezoneIgnoreConverter))]
        public virtual System.DateTime? NewsSpisokDate
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Шапка новости'
        /// </summary>
        [BarsUp.Utils.Display("Шапка новости")]
        [BarsUp.Utils.Attributes.Uid("08b99218-cfdb-40d6-895c-050ae524623c")]
        public virtual System.String NewsSpisokShortName
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Вид'
        /// </summary>
        [BarsUp.Utils.Display("Вид")]
        [BarsUp.Utils.Attributes.Uid("a210a470-f300-43ed-b2de-940e3d2dc228")]
        public virtual Bars.ModulNews.NewsVidPerech? NewsSpisokVid
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Важность'
        /// </summary>
        [BarsUp.Utils.Display("Важность")]
        [BarsUp.Utils.Attributes.Uid("3fb64008-a805-40cc-99bb-ae4d4e445d77")]
        public virtual Bars.ModulNews.NewsPerechVes? NewsSpisokVes
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Полный текст новости'
        /// </summary>
        [BarsUp.Utils.Display("Полный текст новости")]
        [BarsUp.Utils.Attributes.Uid("26ed0b49-06be-4085-829e-1347fd92d12a")]
        public virtual System.String NewsSpisokText
        {
            get;
            set;
        }
    }
}