namespace Bars.ModulNews
{
    using System.Threading.Tasks;
    using System.Diagnostics;
    using BarsUp.Core.Exceptions;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Filter;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.Fias;
    using BarsUp.Modules.FileStorage;
    using BarsUp.Modules.Filter;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils.Annotations;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт сервиса редактора 'Форма для редактирования'
    /// </summary>
    public interface INewsSpisokFormService : IEditorViewService<Bars.ModulNews.NewsSpisok, NewsSpisokFormModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма для редактирования'
    /// </summary>
    public interface INewsSpisokFormServiceHandler : IEditorViewServiceHandler<Bars.ModulNews.NewsSpisok, NewsSpisokFormModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма для редактирования'
    /// </summary>
    public abstract class AbstractNewsSpisokFormServiceHandler : EditorViewServiceHandler<Bars.ModulNews.NewsSpisok, NewsSpisokFormModel>, INewsSpisokFormServiceHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма для редактирования'
    /// </summary>
    public class NewsSpisokFormService : EditorViewService<Bars.ModulNews.NewsSpisok, NewsSpisokFormModel>, INewsSpisokFormService
    {
        public NewsSpisokFormService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override async Task<NewsSpisokFormModel> CreateModelInternalAsync(BaseParams @params)
        {
            await Task.CompletedTask;
            var model = new NewsSpisokFormModel();
            model.NewsSpisokVid = (Bars.ModulNews.NewsVidPerech)1;
            model.NewsSpisokVes = (Bars.ModulNews.NewsPerechVes)1;
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Новости' в модель представления
        /// </summary>
        protected override async Task<NewsSpisokFormModel> MapEntityInternalAsync(Bars.ModulNews.NewsSpisok entity)
        {
            await Task.CompletedTask;
            // создаем экзепляр модели
            var model = new NewsSpisokFormModel();
            model.Id = entity.Id;
            model.NewsSpisokDate = ((System.DateTime? )(entity.NewsSpisokDate));
            model.NewsSpisokShortName = ((System.String)(entity.NewsSpisokShortName));
            model.NewsSpisokVid = ((Bars.ModulNews.NewsVidPerech? )(entity.NewsSpisokVid));
            model.NewsSpisokVes = ((Bars.ModulNews.NewsPerechVes? )(entity.NewsSpisokVes));
            model.NewsSpisokText = ((System.String)(entity.NewsSpisokText));
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Новости' из модели представления		
        /// </summary>
        protected override async Task UnmapEntityInternalAsync(Bars.ModulNews.NewsSpisok entity, NewsSpisokFormModel model, IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            await Task.CompletedTask;
            entity.NewsSpisokDate = model.NewsSpisokDate.GetValueOrDefault();
            entity.NewsSpisokShortName = model.NewsSpisokShortName;
            if (model.NewsSpisokVid.HasValue)
            {
                entity.NewsSpisokVid = model.NewsSpisokVid.Value;
            }
            else
            {
                entity.NewsSpisokVid = null;
            }

            if (model.NewsSpisokVes.HasValue)
            {
                entity.NewsSpisokVes = model.NewsSpisokVes.Value;
            }
            else
            {
                entity.NewsSpisokVes = null;
            }

            entity.NewsSpisokText = model.NewsSpisokText;
        }
    }
}