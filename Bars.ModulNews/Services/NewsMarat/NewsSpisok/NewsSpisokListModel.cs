namespace Bars.ModulNews
{
    using System.Diagnostics;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    /// <summary>
    /// Модель записи реестра 'Новости'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Новости")]
    public class NewsSpisokListModel : DataTransferObject
    {
        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [BarsUp.Utils.Display("Идентификатор типа сущности")]
        [BarsUp.Utils.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [BarsUp.Utils.Display("Наименование редактора сущности")]
        [BarsUp.Utils.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Дата' (псевдоним: NewsSpisokDate)
        /// </summary>
        [BarsUp.Utils.Display("Дата")]
        [BarsUp.Utils.Attributes.Uid("f4ff8e3e-7d59-46f1-8786-0da137996af6")]
        [JsonConverter(typeof(DateTimezoneIgnoreConverter))]
        public virtual System.DateTime? NewsSpisokDate
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Важность' (псевдоним: NewsSpisokVes)
        /// </summary>
        [BarsUp.Utils.Display("Важность")]
        [BarsUp.Utils.Attributes.Uid("0986ed4f-d90b-4b86-8257-b6eccdc7c68c")]
        public virtual Bars.ModulNews.NewsPerechVes? NewsSpisokVes
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Вид' (псевдоним: NewsSpisokVid)
        /// </summary>
        [BarsUp.Utils.Display("Вид")]
        [BarsUp.Utils.Attributes.Uid("e5b50ff6-5565-4b59-a561-87b9bbfc0403")]
        public virtual Bars.ModulNews.NewsVidPerech? NewsSpisokVid
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Шапка новости' (псевдоним: NewsSpisokShortName)
        /// </summary>
        [BarsUp.Utils.Display("Шапка новости")]
        [BarsUp.Utils.Attributes.Uid("e835dbc7-5a7e-4049-9974-977f5bdda787")]
        public virtual System.String NewsSpisokShortName
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [BarsUp.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [BarsUp.Utils.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}