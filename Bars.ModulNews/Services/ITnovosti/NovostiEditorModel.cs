namespace Bars.ModulNews
{
    using BarsUp.Windsor;
    using Newtonsoft.Json;
    using BarsUp.Core.Exceptions;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using BarsUp.Designer.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Новости'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Форма редактирования Новости")]
    public class NovostiEditorModel : DataTransferObject
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public NovostiEditorModel()
        {
        }

        /// <summary>
        /// Свойство 'Заголовок'
        /// </summary>
        [BarsUp.Utils.Display("Заголовок")]
        [BarsUp.Utils.Attributes.Uid("a7179827-0f1d-4f46-ad84-8932f4ae1b3c")]
        public virtual System.String Title
        {
            get;
            set;
        }

        /// <summary>
        /// Статус
        /// </summary>
        [BarsUp.Utils.Display("Статус")]
        [BarsUp.Utils.Attributes.Uid("f64df645-4cfd-43da-9d7b-34920696606a")]
        public virtual Bars.ModulNews.NovostiEditorStateControlModel State
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Содержание'
        /// </summary>
        [BarsUp.Utils.Display("Содержание")]
        [BarsUp.Utils.Attributes.Uid("5e2c5068-9203-4bc0-8098-dc247e58cb29")]
        public virtual System.String BoxOfInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Статус записи
        /// </summary>
        [BarsUp.Utils.Display("Статус записи")]
        [BarsUp.Utils.Attributes.Uid("_State")]
        public virtual BarsUp.Modules.States.StateDTO _State
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Статус'
    /// </summary>
    public class NovostiEditorStateControlModel
    {
        /// <summary>
        /// Свойство 'Новости.Статус.Идентификатор'
        /// </summary>
        [BarsUp.Utils.DisplayAttribute("Новости.Статус.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }
    }
}