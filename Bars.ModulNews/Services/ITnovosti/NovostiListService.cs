namespace Bars.ModulNews
{
    using Castle.Windsor;
    using System.Diagnostics;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Attributes;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Lists;
    using BarsUp.Designer.GeneratedApp.Queries;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System;
    using NHibernate.Linq;
    using BarsUp.Designer.GeneratedApp;
    using System.Threading.Tasks;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Новости'
    /// </summary>
    public interface INovostiListService : IListViewService<Bars.ModulNews.Novosti, Bars.ModulNews.NovostiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра для сервиса представления 'Реестр Новости'
    /// </summary>
    public interface INovostiListServiceFilter : IListViewServiceFilter<Bars.ModulNews.Novosti, Bars.ModulNews.NovostiListModel, BaseParams>
    {
    }

    /// <summary>
    /// Сервис для представления 'Реестр Новости'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Новости")]
    public class NovostiListService : ListViewService<Bars.ModulNews.Novosti, Bars.ModulNews.NovostiListModel>, INovostiListService
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        public NovostiListService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.ModulNews.Novosti> Filter(IQueryable<Bars.ModulNews.Novosti> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.ModulNews.NovostiListModel> Map(IQueryable<Bars.ModulNews.Novosti> entityQuery)
        {
            var query = DataStore.ReadAll<Bars.ModulNews.Novosti>();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.ModulNews.NovostiListModel{Id = x.Id, _TypeUid = "d35e482c-08e2-4d4e-bd85-6c3af43dd329", Title = ((System.String)(x.Title)), BoxOfInfo = ((System.String)(x.BoxOfInfo)), _State = x.State == null ? null : new BarsUp.Modules.States.StateDTO()
            {Id = x.State.Id, Name = x.State.Name, TypeId = x.State.TypeId}, });
        }

        protected override async Task ProcessResultAsync(IList<Bars.ModulNews.NovostiListModel> elements, BaseParams @params)
        {
            await Task.CompletedTask;
        }
    }
}