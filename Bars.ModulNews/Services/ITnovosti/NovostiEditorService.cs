namespace Bars.ModulNews
{
    using System.Threading.Tasks;
    using System.Diagnostics;
    using BarsUp.Core.Exceptions;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Filter;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.Fias;
    using BarsUp.Modules.FileStorage;
    using BarsUp.Modules.Filter;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils.Annotations;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт сервиса редактора 'Форма редактирования Новости'
    /// </summary>
    public interface INovostiEditorService : IEditorViewService<Bars.ModulNews.Novosti, NovostiEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Новости'
    /// </summary>
    public interface INovostiEditorServiceHandler : IEditorViewServiceHandler<Bars.ModulNews.Novosti, NovostiEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Новости'
    /// </summary>
    public abstract class AbstractNovostiEditorServiceHandler : EditorViewServiceHandler<Bars.ModulNews.Novosti, NovostiEditorModel>, INovostiEditorServiceHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Новости'
    /// </summary>
    public class NovostiEditorService : EditorViewService<Bars.ModulNews.Novosti, NovostiEditorModel>, INovostiEditorService
    {
        public NovostiEditorService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override async Task<NovostiEditorModel> CreateModelInternalAsync(BaseParams @params)
        {
            await Task.CompletedTask;
            var model = new NovostiEditorModel();
            var varStateId = @params.JsonData.GetAs<long? >("State_Id");
            if (varStateId != null)
            {
                var queryState = Container.ResolveDomain<BarsUp.Modules.States.State>().GetAll().Where(x => x.Id == varStateId);
                model.State = queryState.Select(x => new NovostiEditorStateControlModel{Id = x.Id, }).FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Новости' в модель представления
        /// </summary>
        protected override async Task<NovostiEditorModel> MapEntityInternalAsync(Bars.ModulNews.Novosti entity)
        {
            await Task.CompletedTask;
            // создаем экзепляр модели
            var model = new NovostiEditorModel();
            model.Id = entity.Id;
            model.Title = ((System.String)(entity.Title));
            model.State = entity.State == null ? null : new NovostiEditorStateControlModel{Id = (System.Int64? )(entity.Return(p => p.State).Return(p => p.Id)), };
            model.BoxOfInfo = ((System.String)(entity.BoxOfInfo));
            model._State = entity.State == null ? null : new BarsUp.Modules.States.StateDTO()
            {Id = entity.State.Id, Name = entity.State.Name, TypeId = entity.State.TypeId};
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Новости' из модели представления		
        /// </summary>
        protected override async Task UnmapEntityInternalAsync(Bars.ModulNews.Novosti entity, NovostiEditorModel model, IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            await Task.CompletedTask;
            entity.Title = model.Title;
            entity.State = isNested ? await TryGetEntityByIdAsync<BarsUp.Modules.States.State>(model.State?.Id) : await TryReadEntityByIdAsync<BarsUp.Modules.States.State>(model.State?.Id);
            entity.BoxOfInfo = model.BoxOfInfo;
        }
    }
}