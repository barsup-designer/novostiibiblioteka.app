namespace Bars.ModulNews
{
    using System.Diagnostics;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Новости'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Новости")]
    public class NovostiListModel : DataTransferObject
    {
        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [BarsUp.Utils.Display("Идентификатор типа сущности")]
        [BarsUp.Utils.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [BarsUp.Utils.Display("Наименование редактора сущности")]
        [BarsUp.Utils.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Заголовок' (псевдоним: Title)
        /// </summary>
        [BarsUp.Utils.Display("Заголовок")]
        [BarsUp.Utils.Attributes.Uid("4b59cf3a-4cc9-4859-8a94-47a1df031623")]
        public virtual System.String Title
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Содержание' (псевдоним: BoxOfInfo)
        /// </summary>
        [BarsUp.Utils.Display("Содержание")]
        [BarsUp.Utils.Attributes.Uid("329ef1a0-8de0-48c9-8af6-de313ec89219")]
        public virtual System.String BoxOfInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [BarsUp.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [BarsUp.Utils.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }

        /// <summary>
        /// Статус записи
        /// </summary>
        [BarsUp.Utils.Display("Статус записи")]
        [BarsUp.Utils.Attributes.Uid("_State")]
        public virtual BarsUp.Modules.States.StateDTO _State
        {
            get;
            set;
        }
    }
}