namespace Bars.ModulNews
{
    using BarsUp.Windsor;
    using Newtonsoft.Json;
    using BarsUp.Core.Exceptions;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using BarsUp.Designer.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Комментарии'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Форма редактирования Комментарии")]
    public class CommentEditorModel : DataTransferObject
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public CommentEditorModel()
        {
        }

        /// <summary>
        /// Свойство 'Содержание'
        /// </summary>
        [BarsUp.Utils.Display("Содержание")]
        [BarsUp.Utils.Attributes.Uid("cb17c0dd-391d-4e63-ba23-3a42edc5dadb")]
        public virtual System.String Content
        {
            get;
            set;
        }

        /// <summary>
        /// Новость
        /// </summary>
        [BarsUp.Utils.Display("Новость")]
        [BarsUp.Utils.Attributes.Uid("76418904-59e5-4878-b7ac-e0b4b58e592e")]
        public virtual Bars.ModulNews.NovostiListModel News
        {
            get;
            set;
        }
    }
}