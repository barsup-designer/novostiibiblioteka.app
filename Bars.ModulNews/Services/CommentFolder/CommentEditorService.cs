namespace Bars.ModulNews
{
    using System.Threading.Tasks;
    using System.Diagnostics;
    using BarsUp.Core.Exceptions;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Filter;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.Fias;
    using BarsUp.Modules.FileStorage;
    using BarsUp.Modules.Filter;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils.Annotations;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт сервиса редактора 'Форма редактирования Комментарии'
    /// </summary>
    public interface ICommentEditorService : IEditorViewService<Bars.ModulNews.Comment, CommentEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Комментарии'
    /// </summary>
    public interface ICommentEditorServiceHandler : IEditorViewServiceHandler<Bars.ModulNews.Comment, CommentEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Комментарии'
    /// </summary>
    public abstract class AbstractCommentEditorServiceHandler : EditorViewServiceHandler<Bars.ModulNews.Comment, CommentEditorModel>, ICommentEditorServiceHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Комментарии'
    /// </summary>
    public class CommentEditorService : EditorViewService<Bars.ModulNews.Comment, CommentEditorModel>, ICommentEditorService
    {
        public CommentEditorService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override async Task<CommentEditorModel> CreateModelInternalAsync(BaseParams @params)
        {
            await Task.CompletedTask;
            var model = new CommentEditorModel();
            var varNewsId = @params.JsonData.GetAs<long? >("News_Id");
            if (varNewsId != null)
            {
                var _NovostiListService = Container.Resolve<Bars.ModulNews.INovostiListService>();
                model.News = await _NovostiListService.GetByIdAsync(varNewsId.Value);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Комментарии' в модель представления
        /// </summary>
        protected override async Task<CommentEditorModel> MapEntityInternalAsync(Bars.ModulNews.Comment entity)
        {
            await Task.CompletedTask;
            // создаем экзепляр модели
            var model = new CommentEditorModel();
            model.Id = entity.Id;
            model.Content = ((System.String)(entity.Content));
            if (entity.News != null)
            {
                var serviceNovostiList = Container.Resolve<Bars.ModulNews.INovostiListService>();
                model.News = await serviceNovostiList.GetByIdAsync(entity.News.Id);
                Container.Release(serviceNovostiList);
                serviceNovostiList = null;
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Комментарии' из модели представления		
        /// </summary>
        protected override async Task UnmapEntityInternalAsync(Bars.ModulNews.Comment entity, CommentEditorModel model, IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            await Task.CompletedTask;
            entity.Content = model.Content;
            entity.News = isNested ? await TryGetEntityByIdAsync<Bars.ModulNews.Novosti>(model.News?.Id) : await TryReadEntityByIdAsync<Bars.ModulNews.Novosti>(model.News?.Id);
        }
    }
}