namespace Bars.ModulNews
{
    using Castle.Windsor;
    using System.Diagnostics;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Attributes;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Lists;
    using BarsUp.Designer.GeneratedApp.Queries;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System;
    using NHibernate.Linq;
    using BarsUp.Designer.GeneratedApp;
    using System.Threading.Tasks;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Комментарии'
    /// </summary>
    public interface ICommentListService : IListViewService<Bars.ModulNews.Comment, Bars.ModulNews.CommentListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра для сервиса представления 'Реестр Комментарии'
    /// </summary>
    public interface ICommentListServiceFilter : IListViewServiceFilter<Bars.ModulNews.Comment, Bars.ModulNews.CommentListModel, BaseParams>
    {
    }

    /// <summary>
    /// Сервис для представления 'Реестр Комментарии'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Комментарии")]
    public class CommentListService : ListViewService<Bars.ModulNews.Comment, Bars.ModulNews.CommentListModel>, ICommentListService
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        public CommentListService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.ModulNews.Comment> Filter(IQueryable<Bars.ModulNews.Comment> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.ModulNews.CommentListModel> Map(IQueryable<Bars.ModulNews.Comment> entityQuery)
        {
            var query = DataStore.ReadAll<Bars.ModulNews.Comment>();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.ModulNews.CommentListModel{Id = x.Id, _TypeUid = "c5cf4ff3-739e-4aca-8c3f-f09bad0e6128", Content = ((System.String)(x.Content)), });
        }

        protected override async Task ProcessResultAsync(IList<Bars.ModulNews.CommentListModel> elements, BaseParams @params)
        {
            await Task.CompletedTask;
        }
    }
}