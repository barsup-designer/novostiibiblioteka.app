namespace Bars.ModulNews
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.IoC;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;
    using BarsUp.ResourceBundling;

    public partial class Module
    {
        protected virtual void RegisterNavigationProviders()
        {
            Component.For<BarsUp.UI.ExtJs.Compatibility4.IExtJsControllerClientRouteMapRegistrar>().ImplementedBy<Bars.ModulNews.GlavnoeMenjuClientRoute>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterExtJsControllerClientRouteMapRegistrar<Bars.ModulNews.ActionClientRoute>();
            Container.RegisterNavigationProvider<Bars.ModulNews.GlavnoeMenjuNavigation>();
        }
    }
}