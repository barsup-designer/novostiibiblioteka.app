namespace Bars.ModulNews.States
{
    using BarsUp.Modules.States;
    using System.Collections.Generic;

    /// <summary>
    /// Объявление статусов
    /// </summary>
    public class StatesManifest : IStatefulEntitiesManifest
    {
        /// <summary>
        /// Получить все статусные объекты
        /// </summary>
        /// <returns>Список</returns>
        public IEnumerable<StatefulEntityInfo> GetAllInfo()
        {
            var list = new List<StatefulEntityInfo>();
            list.Add(new StatefulEntityInfo("bars.modulnews.novosti", "Новости", typeof(Bars.ModulNews.Novosti)));
            return list.ToArray();
        }
    }
}