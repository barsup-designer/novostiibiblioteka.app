namespace Bars.ModulNews
{
    using BarsUp.Modules.Scripting.Interfaces;
    using BarsUp.Modules.Scripting.Services;
    using System;
    using BarsUp;

    internal class NewsReadLoginsDomainServiceInterceptor : EmptyDomainInterceptor<NewsReadLogins>
    {
        private readonly IScriptExecutor _scriptExecutor;
        public NewsReadLoginsDomainServiceInterceptor(IScriptExecutor scriptExecutor)
        {
            _scriptExecutor = scriptExecutor;
        }

        private void RunScript(NewsReadLogins entity, string scriptResourceName, string eventDisplayName)
        {
            var scriptContext = new BaseScriptContext(Container);
            scriptContext.SetValue("entity", entity);
            try
            {
                _scriptExecutor.RunFromResource(scriptContext, scriptResourceName, typeof(NewsReadLogins).Assembly);
            }
            catch (Exception exc)
            {
                throw new ApplicationException($"Не удалось выполнить скрипт события сущности\"{eventDisplayName}\":<br>" + exc.Message);
            }
        }
    }
}