namespace Bars.ModulNews
{
    using BarsUp.Modules.Scripting.Interfaces;
    using BarsUp.Modules.Scripting.Services;
    using System;
    using BarsUp;

    internal class NovostiDomainServiceInterceptor : EmptyDomainInterceptor<Novosti>
    {
        private readonly IScriptExecutor _scriptExecutor;
        public NovostiDomainServiceInterceptor(IScriptExecutor scriptExecutor)
        {
            _scriptExecutor = scriptExecutor;
        }

        private void RunScript(Novosti entity, string scriptResourceName, string eventDisplayName)
        {
            var scriptContext = new BaseScriptContext(Container);
            scriptContext.SetValue("entity", entity);
            try
            {
                _scriptExecutor.RunFromResource(scriptContext, scriptResourceName, typeof(Novosti).Assembly);
            }
            catch (Exception exc)
            {
                throw new ApplicationException($"Не удалось выполнить скрипт события сущности\"{eventDisplayName}\":<br>" + exc.Message);
            }
        }
    }
}