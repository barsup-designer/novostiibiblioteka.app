namespace Bars.ModulNews
{
    using BarsUp.Utils;
    using BarsUp;

    /// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public partial class ResourceManifest
    {
        /// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name = "container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {
            AddResource(container, "libs\\B4\\autostart\\BarsModulNews.js");
            AddResource(container, "libs\\B4\\controller\\CommentEditor.js");
            AddResource(container, "libs\\B4\\controller\\CommentList.js");
            AddResource(container, "libs\\B4\\controller\\NewsSpisokForm.js");
            AddResource(container, "libs\\B4\\controller\\NewsSpisokList.js");
            AddResource(container, "libs\\B4\\controller\\NovostiEditor.js");
            AddResource(container, "libs\\B4\\controller\\NovostiList.js");
            AddResource(container, "libs\\B4\\CustomVTypes.js");
            AddResource(container, "libs\\B4\\view\\CommentEditor.js");
            AddResource(container, "libs\\B4\\view\\CommentList.js");
            AddResource(container, "libs\\B4\\view\\NewsSpisokForm.js");
            AddResource(container, "libs\\B4\\view\\NewsSpisokList.js");
            AddResource(container, "libs\\B4\\view\\NovostiEditor.js");
            AddResource(container, "libs\\B4\\view\\NovostiList.js");
        }

        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");
            container.Add(webPath, "Bars.ModulNews.dll/Bars.ModulNews.{0}".FormatUsing(resourceName));
        }
    }
}