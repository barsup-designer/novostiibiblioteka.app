Ext.define('B4.controller.NovostiList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.NovostiList',
        'B4.model.NovostiListModel',
        'B4.aspects.StateContextMenu'],
    // псевдоним класса реестра
    viewAlias: 'rms-novostilist',
    viewUid: '54c2505e-a1df-4c2d-8bb1-34419b9396d9',
    viewDataName: 'NovostiList',
    // набор описаний действий реестра
    actions: {},
    aspects: [{
        'gridSelector': 'rms-novostilist',
        'menuSelector': 'rmsNovostiListStateMenu',
        'name': 'rmsNovostiListStateAspect',
        'stateType': 'bars.modulnews.novosti',
        'xtype': 'b4_state_contextmenu'
    }],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-novostilist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
    },
});