Ext.define('B4.controller.CommentList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.CommentList',
        'B4.model.CommentListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-commentlist',
    viewUid: '4ca9b29e-01e1-4579-bfc3-6be85a44f838',
    viewDataName: 'CommentList',
    // набор описаний действий реестра
    actions: {},
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-commentlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
    },
});