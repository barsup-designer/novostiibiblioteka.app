Ext.define('B4.controller.NewsSpisokList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.NewsSpisokList',
        'B4.model.NewsSpisokListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-newsspisoklist',
    viewUid: 'ae0be2d8-45f1-4f93-9be1-8226746b41d4',
    viewDataName: 'NewsSpisokList',
    // набор описаний действий реестра
    actions: {
        'Addition-NewsSpisokForm-InWindow': {
            'editor': 'NewsSpisokForm',
            'editorAlias': 'rms-newsspisokform',
            'editorUid': 'd18908b5-b6ac-4657-b5c4-48e3a4f2e5c6',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-NewsSpisokForm-InWindow': {
            'editor': 'NewsSpisokForm',
            'editorAlias': 'rms-newsspisokform',
            'editorUid': 'd18908b5-b6ac-4657-b5c4-48e3a4f2e5c6',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-newsspisoklist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    // Не открываем редактирование, если спрятан верхний тулбар
                    if (sender.panel.dockedItems && !sender.panel.dockedItems.items.filter(x => x.dock == 'top' && x.xtype == 'toolbar').length) return;
                    this.defaultEditActionHandler(false, 'Editing-NewsSpisokForm-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-newsspisoklist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
    },
});