Ext.define('B4.view.NovostiEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-novostieditor',
    title: 'Форма редактирования Новости',
    rmsUid: '0a3dc0d8-d165-40bb-a730-1d708b89f1b8',
    requires: [
        'B4.form.PickerField',
        'BarsUp.versioning.plugin.PickerField'],
    statics: {
        windowSize: {}
    },
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Novosti_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Title',
        'modelProperty': 'Novosti_Title',
        'type': 'TextField'
    }, {
        'dataIndex': 'State',
        'modelProperty': 'Novosti_State',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'BoxOfInfo',
        'modelProperty': 'Novosti_BoxOfInfo',
        'type': 'TextAreaField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'items': [{
                'iconCls': 'fas fs-exchange-alt',
                'itemId': 'btnState',
                'menu': [],
                'text': 'Статус',
                'xtype': 'button'
            }],
            'xtype': 'buttongroup'
        }, {
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '0a3dc0d8-d165-40bb-a730-1d708b89f1b8-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NovostiEditor-container',
        'layout': {
            'type': 'anchor'
        },
        'region': 'Center',
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Заголовок',
                    'labelAlign': 'top',
                    'modelProperty': 'Novosti_Title',
                    'name': 'Title',
                    'rmsUid': 'a7179827-0f1d-4f46-ad84-8932f4ae1b3c',
                    'xtype': 'textfield'
                }, {
                    'anchor': '100%',
                    'columns': [{
                        'dataIndex': 'Id',
                        'flex': 1,
                        'text': 'Идентификатор'
                    }],
                    'editable': false,
                    'fieldLabel': 'Статус',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'maximizable': true,
                    'model': null,
                    'modelProperty': 'Novosti_State',
                    'name': 'State',
                    'plugins': [{
                        'ptype': 'versioningpickerfieldplugin'
                    }],
                    'rmsUid': 'f64df645-4cfd-43da-9d7b-34920696606a',
                    'textProperty': 'Id',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Статус',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'dd86ebeb-68c1-4eb9-97a0-a48be755f63a',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '0f046c25-6a73-4bd7-89bd-6e493b2c6774',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Содержание',
                    'labelAlign': 'top',
                    'modelProperty': 'Novosti_BoxOfInfo',
                    'name': 'BoxOfInfo',
                    'rmsUid': '5e2c5068-9203-4bc0-8098-dc247e58cb29',
                    'xtype': 'textarea'
                }],
                'layout': 'anchor',
                'rmsUid': '43fcae0f-1db2-4650-9e7b-0a1ea7826763',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'd47edc86-fd57-4466-93e8-e55ecb663943',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Novosti_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Title'));
        } else {}
        return res;
    },
});