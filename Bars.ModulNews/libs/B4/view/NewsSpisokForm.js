Ext.define('B4.view.NewsSpisokForm', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-newsspisokform',
    title: 'Форма для редактирования',
    rmsUid: 'd18908b5-b6ac-4657-b5c4-48e3a4f2e5c6',
    requires: [
        'B4.enums.NewsPerechVes',
        'B4.enums.NewsVidPerech',
        'B4.form.EnumCombo'],
    statics: {
        windowSize: {}
    },
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'NewsSpisok_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'NewsSpisokDate',
        'modelProperty': 'NewsSpisok_NewsSpisokDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'NewsSpisokShortName',
        'modelProperty': 'NewsSpisok_NewsSpisokShortName',
        'type': 'TextField'
    }, {
        'dataIndex': 'NewsSpisokVid',
        'modelProperty': 'NewsSpisok_NewsSpisokVid',
        'type': 'DropdownField'
    }, {
        'dataIndex': 'NewsSpisokVes',
        'modelProperty': 'NewsSpisok_NewsSpisokVes',
        'type': 'DropdownField'
    }, {
        'dataIndex': 'NewsSpisokText',
        'modelProperty': 'NewsSpisok_NewsSpisokText',
        'type': 'TextAreaField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'd18908b5-b6ac-4657-b5c4-48e3a4f2e5c6-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'NewsSpisokForm-container',
        'layout': {
            'type': 'anchor'
        },
        'region': 'Center',
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'fieldLabel': 'Атрибуты',
            'items': [{
                'anchor': '20%',
                'fieldLabel': 'Дата',
                'format': 'd.m.Y',
                'labelAlign': 'top',
                'modelProperty': 'NewsSpisok_NewsSpisokDate',
                'name': 'NewsSpisokDate',
                'rmsUid': '889f26e4-3fca-4073-8b20-9ba1a0c99c3e',
                'xtype': 'datefield'
            }, {
                'anchor': '100%',
                'fieldLabel': 'Шапка новости',
                'labelAlign': 'top',
                'modelProperty': 'NewsSpisok_NewsSpisokShortName',
                'name': 'NewsSpisokShortName',
                'rmsUid': '08b99218-cfdb-40d6-895c-050ae524623c',
                'xtype': 'textfield'
            }, {
                'anchor': '100%',
                'enumName': 'B4.enums.NewsVidPerech',
                'fieldLabel': 'Вид новости',
                'labelAlign': 'top',
                'modelProperty': 'NewsSpisok_NewsSpisokVid',
                'name': 'NewsSpisokVid',
                'queryMode': 'local',
                'rmsUid': 'a210a470-f300-43ed-b2de-940e3d2dc228',
                'xtype': 'b4enumcombo'
            }, {
                'anchor': '30%',
                'enumName': 'B4.enums.NewsPerechVes',
                'fieldLabel': 'Важность',
                'labelAlign': 'top',
                'modelProperty': 'NewsSpisok_NewsSpisokVes',
                'name': 'NewsSpisokVes',
                'queryMode': 'local',
                'rmsUid': '3fb64008-a805-40cc-99bb-ae4d4e445d77',
                'xtype': 'b4enumcombo'
            }],
            'layout': {
                'align': 'begin',
                'type': null
            },
            'margin': '5 8 0 5',
            'region': 'Center',
            'rmsUid': '5304ffb4-fd76-46e2-a9e4-51cf2bb08f4a',
            'title': 'Атрибуты',
            'xtype': 'fieldset'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'dockedItems': [],
                    'fieldLabel': 'Группа',
                    'items': [{
                        'anchor': '100%',
                        'fieldLabel': 'Полный текст новости',
                        'height': 150,
                        'labelAlign': 'top',
                        'maxLength': 19996,
                        'maxLengthText': 'Превышена максимальная длина',
                        'minLength': 0,
                        'minLengthText': 'Текст короче минимальной длины',
                        'modelProperty': 'NewsSpisok_NewsSpisokText',
                        'name': 'NewsSpisokText',
                        'rmsUid': '26ed0b49-06be-4085-829e-1347fd92d12a',
                        'xtype': 'textarea'
                    }],
                    'layout': {
                        'align': 'begin',
                        'type': null
                    },
                    'margin': '5 8 5 5',
                    'region': 'Center',
                    'rmsUid': '6473ed47-37c4-4b11-a1b4-7857e35148d1',
                    'title': 'Группа',
                    'xtype': 'fieldset'
                }],
                'layout': 'anchor',
                'rmsUid': '47d8fd5c-6e81-42e3-a2c3-ec8620dfff55',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '0ecb5804-6d07-4ae4-b5e0-e60851ddd529',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'NewsSpisok_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});