Ext.define('B4.view.NewsSpisokList', {
    'alias': 'widget.rms-newsspisoklist',
    'dataSourceUid': '3795a829-583b-461d-99db-978cb530d0f1',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.NewsSpisokListModel',
    'stateful': false,
    'title': 'Новости',
    requires: [
        'B4.enums.NewsPerechVes',
        'B4.enums.NewsVidPerech',
        'B4.model.NewsSpisokListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'BarsUp.filter.ExtGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'ee00a171-0311-4822-bf44-63e3466b287c',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-NewsSpisokForm-InWindow',
        'rmsUid': '9f4fd7cb-310e-46d4-bbdf-e61a9774d415',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-NewsSpisokForm-InWindow',
        'rmsUid': 'e9985281-c9ec-4cc0-97d0-1f9e1e2f32e0',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': 'd2294ddc-ca2e-46a0-8d87-a856469ffbd7',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'NewsSpisokDate',
                'dataIndexAbsoluteUid': 'FieldPath://3795a829-583b-461d-99db-978cb530d0f1$e9c2712d-2338-4e80-b670-94436fff03c8',
                'decimalPrecision': 2,
                'filter': true,
                'ignoreTimeZoneInFilter': true,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'f4ff8e3e-7d59-46f1-8786-0da137996af6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата',
                'width': 80,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'NewsSpisokVes',
                'dataIndexAbsoluteUid': 'FieldPath://3795a829-583b-461d-99db-978cb530d0f1$f6efb235-a186-4c24-8966-820a9fd40c33',
                'decimalPrecision': 2,
                'enumName': 'B4.enums.NewsPerechVes',
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '0986ed4f-d90b-4b86-8257-b6eccdc7c68c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Важность',
                'width': 100,
                'xtype': 'b4enumcolumn'
            }, {
                'dataIndex': 'NewsSpisokVid',
                'dataIndexAbsoluteUid': 'FieldPath://3795a829-583b-461d-99db-978cb530d0f1$b4259931-a8b0-44b8-a41d-1c3b2122834d',
                'decimalPrecision': 2,
                'enumName': 'B4.enums.NewsVidPerech',
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e5b50ff6-5565-4b59-a561-87b9bbfc0403',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Вид',
                'width': 150,
                'xtype': 'b4enumcolumn'
            }, {
                'dataIndex': 'NewsSpisokShortName',
                'dataIndexAbsoluteUid': 'FieldPath://3795a829-583b-461d-99db-978cb530d0f1$6375a39f-f8aa-4386-83c3-bbb8697ff911',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'e835dbc7-5a7e-4049-9974-977f5bdda787',
                'sortable': true,
                'text': 'Шапка новости',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'gridfilters',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});