Ext.define('B4.view.NovostiList', {
    'alias': 'widget.rms-novostilist',
    'dataSourceUid': 'd35e482c-08e2-4d4e-bd85-6c3af43dd329',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.NovostiListModel',
    'stateful': false,
    'title': 'Реестр Новости',
    requires: [
        'B4.form.StateContextMenuColumn',
        'B4.model.NovostiListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'BarsUp.filter.ExtGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '4c63e7b6-7f73-484b-b89e-27e682183a98',
        'text': 'Обновить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': '_State',
                'stateTypeId': 'bars.modulnews.novosti',
                'xtype': 'b4statecontextmenucolumn'
            }, {
                'dataIndex': 'Title',
                'dataIndexAbsoluteUid': 'FieldPath://d35e482c-08e2-4d4e-bd85-6c3af43dd329$bdeb455c-dcc7-475a-aaf0-21b2508af751',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4b59cf3a-4cc9-4859-8a94-47a1df031623',
                'sortable': true,
                'text': 'Заголовок',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'BoxOfInfo',
                'dataIndexAbsoluteUid': 'FieldPath://d35e482c-08e2-4d4e-bd85-6c3af43dd329$08890461-ec07-4287-a0d7-d2add0cc8c8f',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '329ef1a0-8de0-48c9-8af6-de313ec89219',
                'sortable': true,
                'text': 'Содержание',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'gridfilters',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});