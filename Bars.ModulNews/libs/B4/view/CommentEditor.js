Ext.define('B4.view.CommentEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-commenteditor',
    title: 'Форма редактирования Комментарии',
    rmsUid: 'ff5f6a04-41cc-4f5f-a057-299d09823f80',
    requires: [
        'B4.form.PickerField',
        'B4.model.NovostiListModel',
        'B4.view.NovostiList',
        'BarsUp.versioning.plugin.PickerField'],
    statics: {
        windowSize: {}
    },
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Comment_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Content',
        'modelProperty': 'Comment_Content',
        'type': 'TextAreaField'
    }, {
        'dataIndex': 'News',
        'modelProperty': 'Comment_News',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'ff5f6a04-41cc-4f5f-a057-299d09823f80-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'CommentEditor-container',
        'layout': {
            'type': 'anchor'
        },
        'region': 'Center',
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Содержание',
                    'labelAlign': 'top',
                    'modelProperty': 'Comment_Content',
                    'name': 'Content',
                    'rmsUid': 'cb17c0dd-391d-4e63-ba23-3a42edc5dadb',
                    'xtype': 'textarea'
                }],
                'layout': 'anchor',
                'rmsUid': 'b0403fa3-5c5a-4821-b569-df5dd64d33b7',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'd5c295dd-1c81-41e3-a4ac-0e80010427a2',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Новость',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.NovostiList',
                    'listViewCtl': 'B4.controller.NovostiList',
                    'model': 'B4.model.NovostiListModel',
                    'modelProperty': 'Comment_News',
                    'name': 'News',
                    'plugins': [{
                        'ptype': 'versioningpickerfieldplugin'
                    }],
                    'rmsUid': '76418904-59e5-4878-b7ac-e0b4b58e592e',
                    'textProperty': 'Title',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Новость',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '3dea008c-d4c4-4932-af4a-81403fad6c0c',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '49c16a28-ea04-4da7-bd81-4efed1db3f32',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Comment_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});