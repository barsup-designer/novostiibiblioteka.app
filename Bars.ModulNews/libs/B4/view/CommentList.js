Ext.define('B4.view.CommentList', {
    'alias': 'widget.rms-commentlist',
    'dataSourceUid': 'c5cf4ff3-739e-4aca-8c3f-f09bad0e6128',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.CommentListModel',
    'stateful': false,
    'title': 'Реестр Комментарии',
    requires: [
        'B4.model.CommentListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'BarsUp.filter.ExtGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'ebb8b5f1-ad17-43f5-9162-63a98dcf0ae0',
        'text': 'Обновить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Content',
                'dataIndexAbsoluteUid': 'FieldPath://c5cf4ff3-739e-4aca-8c3f-f09bad0e6128$59076be0-40bc-4b2a-ac6a-688fa656c319',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '97ce32c1-4c87-4e9d-a849-29c48bb0b3df',
                'sortable': true,
                'text': 'Содержание',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'gridfilters',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});