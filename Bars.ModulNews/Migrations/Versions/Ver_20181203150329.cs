namespace Bars.ModulNews.Migrations
{
    using BarsUp.Designer.GeneratedApp.Migrations;
    using BarsUp.Ecm7.Framework;
    using BarsUp.Modules.NH.Migrations.DatabaseExtensions;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Modules.PostgreSql.Migrations;
    using BarsUp.Modules.PostgreSql;
    using BarsUp.Modules.Versioning.Extensions;
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System;

    /// <summary>
    /// Миграция 2018.12.03.15-03-29
    /// </summary>
    [BarsUp.Ecm7.Framework.Migration("2018.12.03.15-03-29")]
    public class Ver20181203150329 : BaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void Up()
        {
            EnsureDbTypes();
            Database.AddTable("COMMENT");
            Database.ExecuteNonQuery("COMMENT ON TABLE COMMENT is E\'\"Комментарии\"\'");
            Database.AddTable("NOVOSTI");
            Database.ExecuteNonQuery("COMMENT ON TABLE NOVOSTI is E\'\"Новости\"\'");
            Database.AddColumn("COMMENT", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMMENT.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("COMMENT", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMMENT.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("COMMENT", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMMENT.object_edit_date is E\'\"Дата изменения\"\'");
            Database.AddColumn("COMMENT", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMMENT.object_version is E\'\"Версия\"\'");
            Database.AddColumn("COMMENT", new Column("content", DbType.String.AsColumnType().WithLength(1073741823), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMMENT.content is E\'\"Содержание\"\'");
            Database.AddColumn("COMMENT", new Column("news", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMMENT.news is E\'\"Новость\"\'");
            Database.AddColumn("NOVOSTI", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NOVOSTI.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("NOVOSTI", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NOVOSTI.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("NOVOSTI", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NOVOSTI.object_edit_date is E\'\"Дата изменения\"\'");
            Database.AddColumn("NOVOSTI", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NOVOSTI.object_version is E\'\"Версия\"\'");
            Database.AddColumn("NOVOSTI", new Column("title", DbType.String.AsColumnType().WithLength(100), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NOVOSTI.title is E\'\"Заголовок\"\'");
            Database.AddColumn("NOVOSTI", new Column("boxofinfo", DbType.String.AsColumnType().WithLength(1073741823), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NOVOSTI.boxofinfo is E\'\"Содержание\"\'");
            Database.AddColumn("NOVOSTI", new Column("state", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NOVOSTI.state is E\'\"Статус\"\'");
            Database.ChangeColumnNotNullable("COMMENT", "content", true);
            Database.ChangeColumnNotNullable("COMMENT", "news", true);
            Database.AddForeignKey("FK_46A59081765D7D52CF9825C3FC9EBEC9", "COMMENT", "NEWS", "NOVOSTI", "Id");
            Database.ExecuteNonQuery("COMMENT ON CONSTRAINT FK_46A59081765D7D52CF9825C3FC9EBEC9 on COMMENT is E\'\"Комментарии.Новость -> Новости\"\'");
            Database.AddForeignKey("FK_0DEF2D1D60296625B62756B8521FF718", "NOVOSTI", "STATE", "B4_STATE", "Id");
            Database.ExecuteNonQuery("COMMENT ON CONSTRAINT FK_0DEF2D1D60296625B62756B8521FF718 on NOVOSTI is E\'\"Новости.Статус -> Статус\"\'");
            Database.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS IND_F24179F1D2A206A2A58086D7FB0D2BA0 ON COMMENT (NEWS)");
            Database.ExecuteNonQuery("COMMENT ON INDEX IND_F24179F1D2A206A2A58086D7FB0D2BA0 is E\'\"Комментарии : Новость\"\'");
            Database.ChangeColumnNotNullable("NOVOSTI", "title", true);
            Database.ChangeColumnNotNullable("NOVOSTI", "boxofinfo", true);
            Database.ChangeColumnNotNullable("NOVOSTI", "state", true);
            Database.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS IND_863E7614202FC7C6481FFCB4631AF1E4 ON NOVOSTI (STATE)");
            Database.ExecuteNonQuery("COMMENT ON INDEX IND_863E7614202FC7C6481FFCB4631AF1E4 is E\'\"Новости : Статус\"\'");
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void Down()
        {
            EnsureDbTypes();
            Database.RemoveIndex("IND_863E7614202FC7C6481FFCB4631AF1E4", "NOVOSTI");
            Database.ChangeColumnNotNullable("NOVOSTI", "state", false);
            Database.ChangeColumnNotNullable("NOVOSTI", "boxofinfo", false);
            Database.ChangeColumnNotNullable("NOVOSTI", "title", false);
            Database.RemoveIndex("IND_F24179F1D2A206A2A58086D7FB0D2BA0", "COMMENT");
            Database.RemoveConstraint("NOVOSTI", "FK_0DEF2D1D60296625B62756B8521FF718");
            Database.RemoveConstraint("COMMENT", "FK_46A59081765D7D52CF9825C3FC9EBEC9");
            Database.ChangeColumnNotNullable("COMMENT", "news", false);
            Database.ChangeColumnNotNullable("COMMENT", "content", false);
            Database.RemoveColumn("NOVOSTI", "state");
            Database.RemoveColumn("NOVOSTI", "boxofinfo");
            Database.RemoveColumn("NOVOSTI", "title");
            Database.RemoveColumn("NOVOSTI", "object_version");
            Database.RemoveColumn("NOVOSTI", "object_edit_date");
            Database.RemoveColumn("NOVOSTI", "object_create_date");
            Database.RemoveColumn("COMMENT", "news");
            Database.RemoveColumn("COMMENT", "content");
            Database.RemoveColumn("COMMENT", "object_version");
            Database.RemoveColumn("COMMENT", "object_edit_date");
            Database.RemoveColumn("COMMENT", "object_create_date");
            Database.RemoveTable("NOVOSTI");
            Database.RemoveTable("COMMENT");
        }
    }
}