namespace Bars.ModulNews.Migrations
{
    using BarsUp.Designer.GeneratedApp.Migrations;
    using BarsUp.Ecm7.Framework;
    using BarsUp.Modules.NH.Migrations.DatabaseExtensions;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Modules.PostgreSql.Migrations;
    using BarsUp.Modules.PostgreSql;
    using BarsUp.Modules.Versioning.Extensions;
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System;

    /// <summary>
    /// Миграция 2019.02.14.10-07-39
    /// </summary>
    [BarsUp.Ecm7.Framework.Migration("2019.02.14.10-07-39")]
    public class Ver20190214100739 : BaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void Up()
        {
            EnsureDbTypes();
            Database.AddTable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"});
            Database.ExecuteNonQuery("COMMENT ON TABLE NEWSREADLOGINS is E\'\"Новости прочитанные пользователем\"\'");
            Database.AddTable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"});
            Database.ExecuteNonQuery("COMMENT ON TABLE NEWSSPISOK is E\'\"Новости (Сущность списка новостей)\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSREADLOGINS.id is E\'\"Идентификатор\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSREADLOGINS.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSREADLOGINS.object_edit_date is E\'\"Дата изменения\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSREADLOGINS.object_version is E\'\"Версия\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, new Column("newsspisok", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSREADLOGINS.newsspisok is E\'\"Новость\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, new Column("newsreadloginslogins", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSREADLOGINS.newsreadloginslogins is E\'\"Пользователь\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, new Column("newsreadloginsdate", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSREADLOGINS.newsreadloginsdate is E\'\"Дата прочтения\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.id is E\'\"Идентификатор\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.object_edit_date is E\'\"Дата изменения\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.object_version is E\'\"Версия\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("newsspisokdate", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.newsspisokdate is E\'\"Дата\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("newsspisokves", DbType.Int64.AsColumnType(), ColumnProperty.None, 1));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.newsspisokves is E\'\"Важность\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("newsspisokvid", DbType.Int64.AsColumnType(), ColumnProperty.None, 1));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.newsspisokvid is E\'\"Вид\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("newsspisokshortname", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.newsspisokshortname is E\'\"Шапка новости\"\'");
            Database.AddColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, new Column("newsspisoktext", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN NEWSSPISOK.newsspisoktext is E\'\"Полный текст новости\"\'");
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_create_date", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_edit_date", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_version", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsspisok", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsreadloginslogins", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsreadloginsdate", false);
            Database.AddForeignKey("FK_6D99E6743A55B62EAE46C20288B65BBB", new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "NEWSSPISOK", new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "Id");
            Database.ExecuteNonQuery("COMMENT ON CONSTRAINT FK_6D99E6743A55B62EAE46C20288B65BBB on public.NEWSREADLOGINS is E\'\"Новости прочитанные пользователем.Новость -> Новости\"\'");
            Database.AddForeignKey("FK_7E39C6079075E97F77BE207E57A512B2", new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "NEWSREADLOGINSLOGINS", new SchemaQualifiedObjectName{Schema = "public", Name = "B4_USER"}, "Id");
            Database.ExecuteNonQuery("COMMENT ON CONSTRAINT FK_7E39C6079075E97F77BE207E57A512B2 on public.NEWSREADLOGINS is E\'\"Новости прочитанные пользователем.Пользователь -> Пользователь\"\'");
            Database.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS IND_5AF98CFF5DFA12EBB1A7C944997F6381 ON NEWSREADLOGINS (NEWSSPISOK)");
            Database.ExecuteNonQuery("COMMENT ON INDEX public.IND_5AF98CFF5DFA12EBB1A7C944997F6381 is E\'\"Новости прочитанные пользователем : Новость\"\'");
            Database.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS IND_DBE5361B9631269F75CF582ECBB85AB3 ON NEWSREADLOGINS (NEWSREADLOGINSLOGINS)");
            Database.ExecuteNonQuery("COMMENT ON INDEX public.IND_DBE5361B9631269F75CF582ECBB85AB3 is E\'\"Новости прочитанные пользователем : Пользователь\"\'");
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_create_date", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_edit_date", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_version", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokdate", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokves", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokvid", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokshortname", false);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisoktext", false);
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void Down()
        {
            EnsureDbTypes();
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisoktext", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokshortname", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokvid", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokves", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokdate", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_version", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_edit_date", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_create_date", true);
            Database.RemoveIndex("IND_DBE5361B9631269F75CF582ECBB85AB3", "NEWSREADLOGINS");
            Database.RemoveIndex("IND_5AF98CFF5DFA12EBB1A7C944997F6381", "NEWSREADLOGINS");
            Database.RemoveConstraint("public.NEWSREADLOGINS", "FK_7E39C6079075E97F77BE207E57A512B2");
            Database.RemoveConstraint("public.NEWSREADLOGINS", "FK_6D99E6743A55B62EAE46C20288B65BBB");
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsreadloginsdate", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsreadloginslogins", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsspisok", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_version", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_edit_date", true);
            Database.ChangeColumnNotNullable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_create_date", true);
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisoktext");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokshortname");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokvid");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokves");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "newsspisokdate");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_version");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_edit_date");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"}, "object_create_date");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsreadloginsdate");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsreadloginslogins");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "newsspisok");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_version");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_edit_date");
            Database.RemoveColumn(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"}, "object_create_date");
            Database.RemoveTable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSSPISOK"});
            Database.RemoveTable(new SchemaQualifiedObjectName{Schema = "public", Name = "NEWSREADLOGINS"});
        }
    }
}