namespace Bars.ModulNews
{
    using BarsUp.Designer.GeneratedApp.Reports.ReportContract;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Провайдер контрактов на отчеты
    /// </summary>
    public class ReportContractProvider : IReportContractProvider
    {
        private Lazy<IEnumerable<ReportContractInfo>> _contracts = new Lazy<IEnumerable<ReportContractInfo>>(() =>
        {
            return new List<ReportContractInfo>{};
        }

        );
        /// <summary>
        /// Получить контракты на отчеты
        /// </summary>
        public IEnumerable<ReportContractInfo> GetReportContracts()
        {
            return _contracts.Value;
        }
    }
}