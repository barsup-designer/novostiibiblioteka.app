namespace Bars.ModulNews
{
    using BarsUp.UI.ExtJs.Compatibility4;
    using BarsUp;

    public class GlavnoeMenjuClientRoute : IExtJsControllerClientRouteMapRegistrar
    {
#region Implementation of IExtJsControllerClientRouteMapRegistrar
        /// <summary>
        /// Метод регистрации роута в общей карте
        /// </summary>
        /// <param name = "map"></param>
        public void RegisterRoutes(ExtJsControllerClientRouteMap map)
        {
        }
#endregion
    }
}