namespace Bars.ModulNews
{
    using System.Linq;
    using BarsUp.Utils;
    using BarsUp.DataAccess;
    using BarsUp.IoC;
    using BarsUp.Windsor;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using System.Collections.Generic;
    using System;
    using Castle.Windsor;

    public class GlavnoeMenjuNavigation : INavigationProvider
    {
        public IWindsorContainer Container
        {
            get;
            set;
        }

#region Implementation of INavigationProvider
        /// <summary>
        /// Ключ меню
        /// </summary>
        public string Key => BarsUp.Navigation.BaseMainMenuProvider.MenuKey;
        /// <summary>
        /// Описание меню
        /// </summary>
        public string Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Метод инифиализации
        /// </summary>
        public void Init(MenuItem root)
        {
            MenuItem @ref0 = null;
        }
#endregion
    }
}