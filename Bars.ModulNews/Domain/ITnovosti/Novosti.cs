namespace Bars.ModulNews
{
    using BarsUp.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Новости
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Новости")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d35e482c-08e2-4d4e-bd85-6c3af43dd329")]
    [BarsUp.Utils.Attributes.Uid("d35e482c-08e2-4d4e-bd85-6c3af43dd329")]
    public class Novosti : BarsUp.DataAccess.BaseEntity, BarsUp.Modules.States.IStatefulEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Novosti(): base()
        {
        }

        /// <summary>
        /// Заголовок
        /// </summary>
        [BarsUp.Utils.Display("Заголовок")]
        [BarsUp.Utils.Attributes.Uid("bdeb455c-dcc7-475a-aaf0-21b2508af751")]
        public virtual System.String Title
        {
            get;
            set;
        }

        /// <summary>
        /// Содержание
        /// </summary>
        [BarsUp.Utils.Display("Содержание")]
        [BarsUp.Utils.Attributes.Uid("08890461-ec07-4287-a0d7-d2add0cc8c8f")]
        public virtual System.String BoxOfInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Статус
        /// </summary>
        [BarsUp.Utils.Display("Статус")]
        [BarsUp.Utils.Attributes.Uid("f1e3bc43-7fac-4070-9072-0a9346ae7187")]
        public virtual BarsUp.Modules.States.State State
        {
            get;
            set;
        }
    }
}