namespace Bars.ModulNews
{
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Статус
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Статус")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("1e1232be-30b2-44fb-91c4-e11062a3bfec")]
    [BarsUp.Utils.Attributes.Uid("1e1232be-30b2-44fb-91c4-e11062a3bfec")]
    public enum Status
    {
        /// <summary>
        /// Литерал 1		
        /// </summary>
        [BarsUp.Utils.Display(@"Литерал 1")]
        [BarsUp.Utils.Description(@"")]
        Literal1 = 0,
        /// <summary>
        /// Литерал 2		
        /// </summary>
        [BarsUp.Utils.Display(@"Литерал 2")]
        [BarsUp.Utils.Description(@"")]
        Literal2 = 1
    }
}