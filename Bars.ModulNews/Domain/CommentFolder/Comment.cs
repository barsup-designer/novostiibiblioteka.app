namespace Bars.ModulNews
{
    using BarsUp.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Комментарии
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Комментарии")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("c5cf4ff3-739e-4aca-8c3f-f09bad0e6128")]
    [BarsUp.Utils.Attributes.Uid("c5cf4ff3-739e-4aca-8c3f-f09bad0e6128")]
    public class Comment : BarsUp.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Comment(): base()
        {
        }

        /// <summary>
        /// Содержание
        /// </summary>
        [BarsUp.Utils.Display("Содержание")]
        [BarsUp.Utils.Attributes.Uid("59076be0-40bc-4b2a-ac6a-688fa656c319")]
        public virtual System.String Content
        {
            get;
            set;
        }

        /// <summary>
        /// Новость
        /// </summary>
        [BarsUp.Utils.Display("Новость")]
        [BarsUp.Utils.Attributes.Uid("092856ef-cfcb-4ece-a736-4320e7f95fca")]
        public virtual Bars.ModulNews.Novosti News
        {
            get;
            set;
        }
    }
}