namespace Bars.ModulNews
{
    using BarsUp.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Новости
    /// Сущность списка новостей
    /// </summary>
    [BarsUp.Utils.Display(@"Новости")]
    [BarsUp.Utils.Description(@"Сущность списка новостей")]
    [System.Runtime.InteropServices.GuidAttribute("3795a829-583b-461d-99db-978cb530d0f1")]
    [BarsUp.Utils.Attributes.Uid("3795a829-583b-461d-99db-978cb530d0f1")]
    public class NewsSpisok : BarsUp.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NewsSpisok(): base()
        {
        }

        /// <summary>
        /// Дата
        /// </summary>
        [BarsUp.Utils.Display("Дата")]
        [BarsUp.Utils.Attributes.Uid("e9c2712d-2338-4e80-b670-94436fff03c8")]
        public virtual System.DateTime NewsSpisokDate
        {
            get;
            set;
        }

        /// <summary>
        /// Важность
        /// </summary>
        [BarsUp.Utils.Display("Важность")]
        [BarsUp.Utils.Attributes.Uid("f6efb235-a186-4c24-8966-820a9fd40c33")]
        public virtual Bars.ModulNews.NewsPerechVes? NewsSpisokVes
        {
            get;
            set;
        }

        /// <summary>
        /// Вид
        /// </summary>
        [BarsUp.Utils.Display("Вид")]
        [BarsUp.Utils.Attributes.Uid("b4259931-a8b0-44b8-a41d-1c3b2122834d")]
        public virtual Bars.ModulNews.NewsVidPerech? NewsSpisokVid
        {
            get;
            set;
        }

        /// <summary>
        /// Шапка новости
        /// </summary>
        [BarsUp.Utils.Display("Шапка новости")]
        [BarsUp.Utils.Attributes.Uid("6375a39f-f8aa-4386-83c3-bbb8697ff911")]
        public virtual System.String NewsSpisokShortName
        {
            get;
            set;
        }

        /// <summary>
        /// Полный текст новости
        /// </summary>
        [BarsUp.Utils.Display("Полный текст новости")]
        [BarsUp.Utils.Attributes.Uid("d90b9d40-9711-4199-b29c-0fe74c2c5071")]
        public virtual System.String NewsSpisokText
        {
            get;
            set;
        }
    }
}