namespace Bars.ModulNews
{
    using BarsUp.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Новости прочитанные пользователем
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Новости прочитанные пользователем")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("d43f3562-c793-4371-80f2-26fd18356e9a")]
    [BarsUp.Utils.Attributes.Uid("d43f3562-c793-4371-80f2-26fd18356e9a")]
    public class NewsReadLogins : BarsUp.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public NewsReadLogins(): base()
        {
        }

        /// <summary>
        /// Новость
        /// </summary>
        [BarsUp.Utils.Display("Новость")]
        [BarsUp.Utils.Attributes.Uid("90035aee-ed3a-4096-9e27-edddffec33b7")]
        public virtual Bars.ModulNews.NewsSpisok NewsSpisok
        {
            get;
            set;
        }

        /// <summary>
        /// Пользователь
        /// </summary>
        [BarsUp.Utils.Display("Пользователь")]
        [BarsUp.Utils.Attributes.Uid("074b76e6-058a-42a5-b3b3-eee9865139f5")]
        public virtual BarsUp.Modules.Security.User NewsReadLoginsLogins
        {
            get;
            set;
        }

        /// <summary>
        /// Дата прочтения
        /// </summary>
        [BarsUp.Utils.Display("Дата прочтения")]
        [BarsUp.Utils.Attributes.Uid("8d9e64bb-ca74-4bfa-98a2-534e5d28b6fa")]
        public virtual System.DateTime NewsReadLoginsDate
        {
            get;
            set;
        }
    }
}