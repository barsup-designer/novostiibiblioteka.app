namespace Bars.ModulNews
{
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Перечисление видов новостей
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Перечисление видов новостей")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("36d2578a-c2d6-4c62-a657-deac892ce742")]
    [BarsUp.Utils.Attributes.Uid("36d2578a-c2d6-4c62-a657-deac892ce742")]
    public enum NewsVidPerech
    {
        /// <summary>
        /// Системная		
        /// </summary>
        [BarsUp.Utils.Display(@"Системная")]
        [BarsUp.Utils.Description(@"")]
        NewsVidPerech1 = 1,
        /// <summary>
        /// Нормативная		
        /// </summary>
        [BarsUp.Utils.Display(@"Нормативная")]
        [BarsUp.Utils.Description(@"")]
        NewsVidPerech10 = 10,
        /// <summary>
        /// Прочее		
        /// </summary>
        [BarsUp.Utils.Display(@"Прочее")]
        [BarsUp.Utils.Description(@"")]
        NewsVidPerech20 = 20
    }
}