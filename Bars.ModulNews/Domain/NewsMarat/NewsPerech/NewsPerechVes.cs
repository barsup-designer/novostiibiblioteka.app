namespace Bars.ModulNews
{
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Перачисление важности
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Перачисление важности")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("4418d9fc-e8d9-47e0-8caf-a2338d5df051")]
    [BarsUp.Utils.Attributes.Uid("4418d9fc-e8d9-47e0-8caf-a2338d5df051")]
    public enum NewsPerechVes
    {
        /// <summary>
        /// Высокая		
        /// </summary>
        [BarsUp.Utils.Display(@"Высокая")]
        [BarsUp.Utils.Description(@"")]
        NewsVesPerech1 = 1,
        /// <summary>
        /// Средняя		
        /// </summary>
        [BarsUp.Utils.Display(@"Средняя")]
        [BarsUp.Utils.Description(@"")]
        NewsPerechVes10 = 10,
        /// <summary>
        /// Низкая		
        /// </summary>
        [BarsUp.Utils.Display(@"Низкая")]
        [BarsUp.Utils.Description(@"")]
        NewsPerechVes20 = 20,
        /// <summary>
        /// Неопределена		
        /// </summary>
        [BarsUp.Utils.Display(@"Неопределена")]
        [BarsUp.Utils.Description(@"")]
        NewsPerechVes30 = 30
    }
}