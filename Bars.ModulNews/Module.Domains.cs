namespace Bars.ModulNews
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.IoC;
    using BarsUp.Modules.States.Interceptors;
    using BarsUp.Modules.Versioning.Extensions;
    using BarsUp.Modules.Versioning.Interfaces;
    using BarsUp.Modules.Versioning;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    public partial class Module
    {
        protected virtual void RegisterDomainServices()
        {
            Component.For<BarsUp.Modules.States.IStatefulEntitiesManifest>().ImplementedBy<Bars.ModulNews.States.StatesManifest>().LifestyleTransient().RegisterIn(Container);
            Component.For<BarsUp.DataAccess.INhibernateConfigModifier>().ImplementedBy<Bars.ModulNews.NHibernateConfigurator>().LifestyleTransient().RegisterIn(Container);
            Component.For<BarsUp.IModuleDependencies>().ImplementedBy<Bars.ModulNews.ModuleDependencies>().LifestyleSingleton().RegisterIn(Container);
            Component.For<BarsUp.Modules.NHibernateChangeLog.IAuditLogMapProvider>().ImplementedBy<Bars.ModulNews.AuditLogMapProvider>().LifestyleSingleton().RegisterIn(Container);
            Container.RegisterDomainInterceptor<Bars.ModulNews.Novosti, GenericStatefulEntityInterceptor<Bars.ModulNews.Novosti>>();
            Container.RegisterDomainInterceptor<Comment, CommentDomainServiceInterceptor>();
            Container.RegisterDomainInterceptor<NewsReadLogins, NewsReadLoginsDomainServiceInterceptor>();
            Container.RegisterDomainInterceptor<NewsSpisok, NewsSpisokDomainServiceInterceptor>();
            Container.RegisterDomainInterceptor<Novosti, NovostiDomainServiceInterceptor>();
            Container.RegisterEditorViewService<CommentEditorService>();
            Container.RegisterEditorViewService<NewsSpisokFormService>();
            Container.RegisterEditorViewService<NovostiEditorService>();
            Container.RegisterListViewService<CommentListService>();
            Container.RegisterListViewService<NewsSpisokListService>();
            Container.RegisterListViewService<NovostiListService>();
            NHibernateConfigurator.RegisterAll();
        }
    }
}