namespace Bars.ModulNews
{
    using BarsUp.UI.ExtJs;
    using BarsUp;
    using BarsUp.Modules.Security;
    using BarsUp.UI.ExtJs.Compatibility4;
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System.Linq;
    using MoreLinq;

    public partial class ResourceManifest : ResourceManifestBase
    {
        protected override void AdditionalInit(IResourceManifestContainer container)
        {
#region Перечисления
            container.DefineExtJsEnum<Bars.ModulNews.NewsPerechVes>().AlterClassName("NewsPerechVes");
            container.DefineExtJsEnum<Bars.ModulNews.NewsVidPerech>().AlterClassName("NewsVidPerech");
            container.DefineExtJsEnum<Bars.ModulNews.Status>().AlterClassName("Status");
#endregion
#region Модели
            container.DefineExtJsModel<Bars.ModulNews.CommentEditorModel>().ActionMethods(read: "POST").Controller("CommentEditor");
            container.DefineExtJsModel<Bars.ModulNews.NewsSpisokFormModel>().ActionMethods(read: "POST").Controller("NewsSpisokForm");
            container.DefineExtJsModel<Bars.ModulNews.NovostiEditorModel>().ActionMethods(read: "POST").Controller("NovostiEditor");
            container.DefineExtJsModel<BarsUp.Modules.FileStorage.FileInfo>().ActionMethods(read: "POST").Controller("Empty");
            container.DefineExtJsModel<CommentListModel>().ActionMethods(read: "POST").Controller("CommentList");
            container.DefineExtJsModel<NewsSpisokListModel>().ActionMethods(read: "POST").Controller("NewsSpisokList");
            container.DefineExtJsModel<NovostiListModel>().ActionMethods(read: "POST").Controller("NovostiList");
#endregion
        }
    }
}