namespace Bars.ModulNews.Mappings
{
    using BarsUp.DataAccess.ByCode;
    using BarsUp.DataAccess.UserTypes;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Modules.PostgreSql.DataAccess.UserTypes;
    using BarsUp.Modules.Versioning.Map;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;
    using NHibernate;

    /// <summary>
    /// Мапинг сущности Новости
    /// </summary>
    public class NewsSpisokMap : ClassMapping<Bars.ModulNews.NewsSpisok>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public NewsSpisokMap()
        {
            Polymorphism(PolymorphismType.Explicit);
            Table("NEWSSPISOK");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new BarsUp.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: NewsSpisok
            Property(x => x.NewsSpisokDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("NEWSSPISOKDATE");
                }

                );
            }

            );
            Property(x => x.NewsSpisokVes, p =>
            {
                p.Column(cm =>
                {
                    cm.Name("NEWSSPISOKVES");
                    cm.Default(1);
                }

                );
            }

            );
            Property(x => x.NewsSpisokVid, p =>
            {
                p.Column(cm =>
                {
                    cm.Name("NEWSSPISOKVID");
                    cm.Default(1);
                }

                );
            }

            );
            Property(x => x.NewsSpisokShortName, p =>
            {
                p.Column(col =>
                {
                    col.Name("NEWSSPISOKSHORTNAME");
                }

                );
            }

            );
            Property(x => x.NewsSpisokText, p =>
            {
                p.Column(col =>
                {
                    col.Name("NEWSSPISOKTEXT");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
            }

            );
#endregion
        }
    }
}