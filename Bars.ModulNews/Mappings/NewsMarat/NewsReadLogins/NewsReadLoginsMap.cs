namespace Bars.ModulNews.Mappings
{
    using BarsUp.DataAccess.ByCode;
    using BarsUp.DataAccess.UserTypes;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Modules.PostgreSql.DataAccess.UserTypes;
    using BarsUp.Modules.Versioning.Map;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;
    using NHibernate;

    /// <summary>
    /// Мапинг сущности Новости прочитанные пользователем
    /// </summary>
    public class NewsReadLoginsMap : ClassMapping<Bars.ModulNews.NewsReadLogins>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public NewsReadLoginsMap()
        {
            Polymorphism(PolymorphismType.Explicit);
            Table("NEWSREADLOGINS");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new BarsUp.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: NewsReadLogins
            ManyToOne(x => x.NewsSpisok, m =>
            {
                m.Column("NEWSSPISOK");
                m.Cascade(Cascade.Persist);
            }

            );
            ManyToOne(x => x.NewsReadLoginsLogins, m =>
            {
                m.Column("NEWSREADLOGINSLOGINS");
                m.Cascade(Cascade.Persist);
            }

            );
            Property(x => x.NewsReadLoginsDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("NEWSREADLOGINSDATE");
                }

                );
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
            }

            );
#endregion
        }
    }
}