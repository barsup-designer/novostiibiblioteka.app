namespace Bars.Biblioteka.Migrations
{
    using BarsUp.Designer.GeneratedApp.Migrations;
    using BarsUp.Ecm7.Framework;
    using BarsUp.Modules.NH.Migrations.DatabaseExtensions;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Modules.PostgreSql.Migrations;
    using BarsUp.Modules.PostgreSql;
    using BarsUp.Modules.Versioning.Extensions;
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System;

    /// <summary>
    /// Миграция 2018.12.03.15-03-29
    /// </summary>
    [BarsUp.Ecm7.Framework.Migration("2018.12.03.15-03-29")]
    public class Ver20181203150329 : BaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void Up()
        {
            EnsureDbTypes();
            Database.AddTable("AVTOR");
            Database.ExecuteNonQuery("COMMENT ON TABLE AVTOR is E\'\"Автор\"\'");
            Database.AddTable("IZDATELSTVO");
            Database.ExecuteNonQuery("COMMENT ON TABLE IZDATELSTVO is E\'\"Издательство\"\'");
            Database.AddTable("KNIGA");
            Database.ExecuteNonQuery("COMMENT ON TABLE KNIGA is E\'\"Книга\"\'");
            Database.AddColumn("AVTOR", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN AVTOR.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("AVTOR", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN AVTOR.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("AVTOR", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN AVTOR.object_edit_date is E\'\"Дата изменения\"\'");
            Database.AddColumn("AVTOR", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN AVTOR.object_version is E\'\"Версия\"\'");
            Database.AddColumn("AVTOR", new Column("firstname", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN AVTOR.firstname is E\'\"Фамилия\"\'");
            Database.AddColumn("AVTOR", new Column("name", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN AVTOR.name is E\'\"Имя\"\'");
            Database.AddColumn("AVTOR", new Column("secondname", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN AVTOR.secondname is E\'\"Отчество\"\'");
            Database.AddColumn("IZDATELSTVO", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN IZDATELSTVO.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("IZDATELSTVO", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN IZDATELSTVO.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("IZDATELSTVO", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN IZDATELSTVO.object_edit_date is E\'\"Дата изменения\"\'");
            Database.AddColumn("IZDATELSTVO", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN IZDATELSTVO.object_version is E\'\"Версия\"\'");
            Database.AddColumn("IZDATELSTVO", new Column("name", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN IZDATELSTVO.name is E\'\"Название\"\'");
            Database.AddColumn("IZDATELSTVO", new Column("books", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN IZDATELSTVO.books is E\'\"Книги\"\'");
            Database.AddColumn("KNIGA", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KNIGA.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("KNIGA", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KNIGA.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("KNIGA", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KNIGA.object_edit_date is E\'\"Дата изменения\"\'");
            Database.AddColumn("KNIGA", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KNIGA.object_version is E\'\"Версия\"\'");
            Database.AddColumn("KNIGA", new Column("name", DbType.String.AsColumnType().WithLength(2147483647), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KNIGA.name is E\'\"Название\"\'");
            Database.AddColumn("KNIGA", new Column("data", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KNIGA.data is E\'\"Год издания\"\'");
            Database.AddColumn("KNIGA", new Column("publishing", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KNIGA.publishing is E\'\"Издательство\"\'");
            Database.ChangeColumnNotNullable("AVTOR", "firstname", true);
            Database.ChangeColumnNotNullable("AVTOR", "name", true);
            Database.ChangeColumnNotNullable("AVTOR", "secondname", true);
            Database.ChangeColumnNotNullable("IZDATELSTVO", "name", true);
            Database.AddForeignKey("FK_5966AB7911C4A747D2C6B9574BADEC78", "IZDATELSTVO", "BOOKS", "KNIGA", "Id");
            Database.ExecuteNonQuery("COMMENT ON CONSTRAINT FK_5966AB7911C4A747D2C6B9574BADEC78 on IZDATELSTVO is E\'\"Издательство.Книги -> Книга\"\'");
            Database.AddForeignKey("FK_88F62CCA9903430829743C767EFA8CCD", "KNIGA", "PUBLISHING", "TST_PUBLISHER", "Id");
            Database.ExecuteNonQuery("COMMENT ON CONSTRAINT FK_88F62CCA9903430829743C767EFA8CCD on KNIGA is E\'\"Книга.Издательство -> Publisher\"\'");
            Database.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS IND_08550E6C47CEA2704C93064FE67B5135 ON IZDATELSTVO (BOOKS)");
            Database.ExecuteNonQuery("COMMENT ON INDEX IND_08550E6C47CEA2704C93064FE67B5135 is E\'\"Издательство : Книги\"\'");
            Database.ChangeColumnNotNullable("KNIGA", "data", true);
            Database.ChangeColumnNotNullable("KNIGA", "publishing", true);
            Database.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS IND_FFCD5F46A2DF00A7CEAA71B2FD52C20A ON KNIGA (PUBLISHING)");
            Database.ExecuteNonQuery("COMMENT ON INDEX IND_FFCD5F46A2DF00A7CEAA71B2FD52C20A is E\'\"Книга : Издательство\"\'");
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void Down()
        {
            EnsureDbTypes();
            Database.RemoveIndex("IND_FFCD5F46A2DF00A7CEAA71B2FD52C20A", "KNIGA");
            Database.ChangeColumnNotNullable("KNIGA", "publishing", false);
            Database.ChangeColumnNotNullable("KNIGA", "data", false);
            Database.RemoveIndex("IND_08550E6C47CEA2704C93064FE67B5135", "IZDATELSTVO");
            Database.RemoveConstraint("KNIGA", "FK_88F62CCA9903430829743C767EFA8CCD");
            Database.RemoveConstraint("IZDATELSTVO", "FK_5966AB7911C4A747D2C6B9574BADEC78");
            Database.ChangeColumnNotNullable("IZDATELSTVO", "name", false);
            Database.ChangeColumnNotNullable("AVTOR", "secondname", false);
            Database.ChangeColumnNotNullable("AVTOR", "name", false);
            Database.ChangeColumnNotNullable("AVTOR", "firstname", false);
            Database.RemoveColumn("KNIGA", "publishing");
            Database.RemoveColumn("KNIGA", "data");
            Database.RemoveColumn("KNIGA", "name");
            Database.RemoveColumn("KNIGA", "object_version");
            Database.RemoveColumn("KNIGA", "object_edit_date");
            Database.RemoveColumn("KNIGA", "object_create_date");
            Database.RemoveColumn("IZDATELSTVO", "books");
            Database.RemoveColumn("IZDATELSTVO", "name");
            Database.RemoveColumn("IZDATELSTVO", "object_version");
            Database.RemoveColumn("IZDATELSTVO", "object_edit_date");
            Database.RemoveColumn("IZDATELSTVO", "object_create_date");
            Database.RemoveColumn("AVTOR", "secondname");
            Database.RemoveColumn("AVTOR", "name");
            Database.RemoveColumn("AVTOR", "firstname");
            Database.RemoveColumn("AVTOR", "object_version");
            Database.RemoveColumn("AVTOR", "object_edit_date");
            Database.RemoveColumn("AVTOR", "object_create_date");
            Database.RemoveTable("KNIGA");
            Database.RemoveTable("IZDATELSTVO");
            Database.RemoveTable("AVTOR");
        }
    }
}