namespace Bars.Biblioteka
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.IoC;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;
    using BarsUp.ResourceBundling;

    public partial class Module
    {
        protected virtual void RegisterNavigationProviders()
        {
            Container.RegisterExtJsControllerClientRouteMapRegistrar<Bars.Biblioteka.ActionClientRoute>();
        }
    }
}