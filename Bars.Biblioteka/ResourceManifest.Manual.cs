namespace Bars.Biblioteka
{
    using BarsUp.UI.ExtJs;
    using BarsUp;
    using BarsUp.Modules.Security;
    using BarsUp.UI.ExtJs.Compatibility4;
    using BarsUp.Utils;
    using System.Collections.Generic;
    using System.Linq;
    using MoreLinq;

    public partial class ResourceManifest : ResourceManifestBase
    {
        protected override void AdditionalInit(IResourceManifestContainer container)
        {
#region Перечисления
#endregion
#region Модели
            container.DefineExtJsModel<AvtorListModel>().ActionMethods(read: "POST").Controller("AvtorList");
            container.DefineExtJsModel<Bars.Biblioteka.AvtorEditorModel>().ActionMethods(read: "POST").Controller("AvtorEditor");
            container.DefineExtJsModel<Bars.Biblioteka.IzdatelStvoEditorModel>().ActionMethods(read: "POST").Controller("IzdatelStvoEditor");
            container.DefineExtJsModel<Bars.Biblioteka.KnigaEditorModel>().ActionMethods(read: "POST").Controller("KnigaEditor");
            container.DefineExtJsModel<BarsUp.Modules.FileStorage.FileInfo>().ActionMethods(read: "POST").Controller("Empty");
            container.DefineExtJsModel<IzdatelStvoListModel>().ActionMethods(read: "POST").Controller("IzdatelStvoList");
            container.DefineExtJsModel<KnigaListModel>().ActionMethods(read: "POST").Controller("KnigaList");
#endregion
        }
    }
}