namespace Bars.Biblioteka.Mappings
{
    using BarsUp.DataAccess.ByCode;
    using BarsUp.DataAccess.UserTypes;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Modules.PostgreSql.DataAccess.UserTypes;
    using BarsUp.Modules.Versioning.Map;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;
    using NHibernate;

    /// <summary>
    /// Мапинг сущности Автор
    /// </summary>
    public class AvtorMap : ClassMapping<Bars.Biblioteka.Avtor>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public AvtorMap()
        {
            Polymorphism(PolymorphismType.Explicit);
            Table("AVTOR");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new BarsUp.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Avtor
            Property(x => x.Firstname, p =>
            {
                p.Column(col =>
                {
                    col.Name("FIRSTNAME");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.Name, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAME");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.Secondname, p =>
            {
                p.Column(col =>
                {
                    col.Name("SECONDNAME");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
            }

            );
#endregion
        }
    }
}