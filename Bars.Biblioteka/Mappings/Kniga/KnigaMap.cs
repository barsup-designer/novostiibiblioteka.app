namespace Bars.Biblioteka.Mappings
{
    using BarsUp.DataAccess.ByCode;
    using BarsUp.DataAccess.UserTypes;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Modules.PostgreSql.DataAccess.UserTypes;
    using BarsUp.Modules.Versioning.Map;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;
    using NHibernate;

    /// <summary>
    /// Мапинг сущности Книга
    /// </summary>
    public class KnigaMap : ClassMapping<Bars.Biblioteka.Kniga>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public KnigaMap()
        {
            Polymorphism(PolymorphismType.Explicit);
            Table("KNIGA");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new BarsUp.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Kniga
            Property(x => x.name, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAME");
                }

                );
            }

            );
            Property(x => x.Data, p =>
            {
                p.Column(col =>
                {
                    col.Name("DATA");
                }

                );
                p.NotNullable(true);
            }

            );
            ManyToOne(x => x.Publishing, m =>
            {
                m.Column("PUBLISHING");
                m.NotNullable(true);
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
            }

            );
#endregion
        }
    }
}