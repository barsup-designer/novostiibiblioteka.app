namespace Bars.Biblioteka.Mappings
{
    using BarsUp.DataAccess.ByCode;
    using BarsUp.DataAccess.UserTypes;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Modules.PostgreSql.DataAccess.UserTypes;
    using BarsUp.Modules.Versioning.Map;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;
    using NHibernate;

    /// <summary>
    /// Мапинг сущности Издательство
    /// </summary>
    public class IzdatelStvoMap : ClassMapping<Bars.Biblioteka.IzdatelStvo>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public IzdatelStvoMap()
        {
            Polymorphism(PolymorphismType.Explicit);
            Table("IZDATELSTVO");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new BarsUp.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: IzdatelStvo
            Property(x => x.Name, p =>
            {
                p.Column(col =>
                {
                    col.Name("NAME");
                }

                );
                p.NotNullable(true);
            }

            );
            ManyToOne(x => x.Books, m =>
            {
                m.Column("BOOKS");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
            }

            );
#endregion
        }
    }
}