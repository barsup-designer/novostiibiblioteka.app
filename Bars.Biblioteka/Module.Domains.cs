namespace Bars.Biblioteka
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.IoC;
    using BarsUp.Modules.States.Interceptors;
    using BarsUp.Modules.Versioning.Extensions;
    using BarsUp.Modules.Versioning.Interfaces;
    using BarsUp.Modules.Versioning;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    public partial class Module
    {
        protected virtual void RegisterDomainServices()
        {
            Component.For<BarsUp.Modules.States.IStatefulEntitiesManifest>().ImplementedBy<Bars.Biblioteka.States.StatesManifest>().LifestyleTransient().RegisterIn(Container);
            Component.For<BarsUp.DataAccess.INhibernateConfigModifier>().ImplementedBy<Bars.Biblioteka.NHibernateConfigurator>().LifestyleTransient().RegisterIn(Container);
            Component.For<BarsUp.IModuleDependencies>().ImplementedBy<Bars.Biblioteka.ModuleDependencies>().LifestyleSingleton().RegisterIn(Container);
            Component.For<BarsUp.Modules.NHibernateChangeLog.IAuditLogMapProvider>().ImplementedBy<Bars.Biblioteka.AuditLogMapProvider>().LifestyleSingleton().RegisterIn(Container);
            Container.RegisterDomainInterceptor<Avtor, AvtorDomainServiceInterceptor>();
            Container.RegisterDomainInterceptor<IzdatelStvo, IzdatelStvoDomainServiceInterceptor>();
            Container.RegisterDomainInterceptor<Kniga, KnigaDomainServiceInterceptor>();
            Container.RegisterEditorViewService<AvtorEditorService>();
            Container.RegisterEditorViewService<IzdatelStvoEditorService>();
            Container.RegisterEditorViewService<KnigaEditorService>();
            Container.RegisterListViewService<AvtorListService>();
            Container.RegisterListViewService<IzdatelStvoListService>();
            Container.RegisterListViewService<KnigaListService>();
            NHibernateConfigurator.RegisterAll();
        }
    }
}