namespace Bars.Biblioteka
{
    using BarsUp.License;

    /// <summary>
    /// Информация о лицензии
    /// </summary>
    public class LicenceInformation : ILicenseInfo
    {
        public string GetLicenseInfo()
        {
            var result = string.Empty;
            if (BarsUp.License.License.IsInit && BarsUp.License.License.IsActive)
            {
                result = BarsUp.License.License.GetValue("number");
            }

            return "Номер лицензии: " + result;
        }
    }
}