namespace Bars.Biblioteka
{
    using BarsUp.Utils;
    using BarsUp;

    /// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public partial class ResourceManifest
    {
        /// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name = "container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {
            AddResource(container, "libs\\B4\\autostart\\BarsBiblioteka.js");
            AddResource(container, "libs\\B4\\controller\\AvtorEditor.js");
            AddResource(container, "libs\\B4\\controller\\AvtorList.js");
            AddResource(container, "libs\\B4\\controller\\IzdatelStvoEditor.js");
            AddResource(container, "libs\\B4\\controller\\IzdatelStvoList.js");
            AddResource(container, "libs\\B4\\controller\\KnigaEditor.js");
            AddResource(container, "libs\\B4\\controller\\KnigaList.js");
            AddResource(container, "libs\\B4\\CustomVTypes.js");
            AddResource(container, "libs\\B4\\view\\AvtorEditor.js");
            AddResource(container, "libs\\B4\\view\\AvtorList.js");
            AddResource(container, "libs\\B4\\view\\IzdatelStvoEditor.js");
            AddResource(container, "libs\\B4\\view\\IzdatelStvoList.js");
            AddResource(container, "libs\\B4\\view\\KnigaEditor.js");
            AddResource(container, "libs\\B4\\view\\KnigaList.js");
        }

        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");
            container.Add(webPath, "Bars.Biblioteka.dll/Bars.Biblioteka.{0}".FormatUsing(resourceName));
        }
    }
}