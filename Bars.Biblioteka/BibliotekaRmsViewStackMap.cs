namespace Bars.Biblioteka
{
    /// <summary>
    /// Маппинг UID реестров/форм
    /// </summary>
    public class BibliotekaRmsViewStackMap
    {
        public const string AvtorListListView = "e7121741-7046-4e43-bcf0-c9ce70fbcd88";
        public const string IzdatelStvoListListView = "8e4dbc2f-2ea1-4535-804c-f5e84e410d1c";
        public const string KnigaListListView = "320ef596-90b8-403d-b0a4-7141a991e6bf";
        public const string CommentListListView = "4ca9b29e-01e1-4579-bfc3-6be85a44f838";
        public const string NovostiListListView = "54c2505e-a1df-4c2d-8bb1-34419b9396d9";
        public const string NewsSpisokListListView = "ae0be2d8-45f1-4f93-9be1-8226746b41d4";
        public const string AvtorEditorForm = "56975756-c993-4b72-8051-dbed47852d22";
        public const string IzdatelStvoEditorForm = "807113cf-aefc-4ddb-be69-99979012cdb8";
        public const string KnigaEditorForm = "5306625c-c678-41c5-8fae-c0c4f1d52b93";
        public const string CommentEditorForm = "ff5f6a04-41cc-4f5f-a057-299d09823f80";
        public const string NovostiEditorForm = "0a3dc0d8-d165-40bb-a730-1d708b89f1b8";
        public const string NewsSpisokFormForm = "d18908b5-b6ac-4657-b5c4-48e3a4f2e5c6";
    }
}