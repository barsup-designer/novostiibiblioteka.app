namespace Bars.Biblioteka
{
    using BarsUp.Windsor;
    using Newtonsoft.Json;
    using BarsUp.Core.Exceptions;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using BarsUp.Designer.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Автор'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Форма редактирования Автор")]
    public class AvtorEditorModel : DataTransferObject
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public AvtorEditorModel()
        {
        }

        /// <summary>
        /// Свойство 'Фамилия'
        /// </summary>
        [BarsUp.Utils.Display("Фамилия")]
        [BarsUp.Utils.Attributes.Uid("dbe56ed8-e5d9-4726-817d-d7ce70cf9c34")]
        public virtual System.String Firstname
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Имя'
        /// </summary>
        [BarsUp.Utils.Display("Имя")]
        [BarsUp.Utils.Attributes.Uid("d1755db2-fe7c-4d3d-9f6b-512385b49708")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Отчество'
        /// </summary>
        [BarsUp.Utils.Display("Отчество")]
        [BarsUp.Utils.Attributes.Uid("a271bbab-a0f3-49b0-adfc-8c507c5b99dc")]
        public virtual System.String Secondname
        {
            get;
            set;
        }
    }
}