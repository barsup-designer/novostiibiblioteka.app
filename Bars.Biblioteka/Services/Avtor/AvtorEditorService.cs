namespace Bars.Biblioteka
{
    using System.Threading.Tasks;
    using System.Diagnostics;
    using BarsUp.Core.Exceptions;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Filter;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.Fias;
    using BarsUp.Modules.FileStorage;
    using BarsUp.Modules.Filter;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils.Annotations;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт сервиса редактора 'Форма редактирования Автор'
    /// </summary>
    public interface IAvtorEditorService : IEditorViewService<Bars.Biblioteka.Avtor, AvtorEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Автор'
    /// </summary>
    public interface IAvtorEditorServiceHandler : IEditorViewServiceHandler<Bars.Biblioteka.Avtor, AvtorEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Автор'
    /// </summary>
    public abstract class AbstractAvtorEditorServiceHandler : EditorViewServiceHandler<Bars.Biblioteka.Avtor, AvtorEditorModel>, IAvtorEditorServiceHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Автор'
    /// </summary>
    public class AvtorEditorService : EditorViewService<Bars.Biblioteka.Avtor, AvtorEditorModel>, IAvtorEditorService
    {
        public AvtorEditorService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override async Task<AvtorEditorModel> CreateModelInternalAsync(BaseParams @params)
        {
            await Task.CompletedTask;
            var model = new AvtorEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Автор' в модель представления
        /// </summary>
        protected override async Task<AvtorEditorModel> MapEntityInternalAsync(Bars.Biblioteka.Avtor entity)
        {
            await Task.CompletedTask;
            // создаем экзепляр модели
            var model = new AvtorEditorModel();
            model.Id = entity.Id;
            model.Firstname = ((System.String)(entity.Firstname));
            model.Name = ((System.String)(entity.Name));
            model.Secondname = ((System.String)(entity.Secondname));
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Автор' из модели представления		
        /// </summary>
        protected override async Task UnmapEntityInternalAsync(Bars.Biblioteka.Avtor entity, AvtorEditorModel model, IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            await Task.CompletedTask;
            entity.Firstname = model.Firstname;
            entity.Name = model.Name;
            entity.Secondname = model.Secondname;
        }
    }
}