namespace Bars.Biblioteka
{
    using Castle.Windsor;
    using System.Diagnostics;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Attributes;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Lists;
    using BarsUp.Designer.GeneratedApp.Queries;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System;
    using NHibernate.Linq;
    using BarsUp.Designer.GeneratedApp;
    using System.Threading.Tasks;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Автор'
    /// </summary>
    public interface IAvtorListService : IListViewService<Bars.Biblioteka.Avtor, Bars.Biblioteka.AvtorListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра для сервиса представления 'Реестр Автор'
    /// </summary>
    public interface IAvtorListServiceFilter : IListViewServiceFilter<Bars.Biblioteka.Avtor, Bars.Biblioteka.AvtorListModel, BaseParams>
    {
    }

    /// <summary>
    /// Сервис для представления 'Реестр Автор'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Автор")]
    public class AvtorListService : ListViewService<Bars.Biblioteka.Avtor, Bars.Biblioteka.AvtorListModel>, IAvtorListService
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        public AvtorListService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Biblioteka.Avtor> Filter(IQueryable<Bars.Biblioteka.Avtor> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Biblioteka.AvtorListModel> Map(IQueryable<Bars.Biblioteka.Avtor> entityQuery)
        {
            var query = DataStore.ReadAll<Bars.Biblioteka.Avtor>();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Biblioteka.AvtorListModel{Id = x.Id, _TypeUid = "2143cebd-96b8-4103-9ed0-56afd273400e", Firstname = ((System.String)(x.Firstname)), Name = ((System.String)(x.Name)), Secondname = ((System.String)(x.Secondname)), });
        }

        protected override async Task ProcessResultAsync(IList<Bars.Biblioteka.AvtorListModel> elements, BaseParams @params)
        {
            await Task.CompletedTask;
        }
    }
}