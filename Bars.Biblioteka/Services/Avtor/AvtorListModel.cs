namespace Bars.Biblioteka
{
    using System.Diagnostics;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Автор'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Автор")]
    public class AvtorListModel : DataTransferObject
    {
        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [BarsUp.Utils.Display("Идентификатор типа сущности")]
        [BarsUp.Utils.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [BarsUp.Utils.Display("Наименование редактора сущности")]
        [BarsUp.Utils.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Фамилия' (псевдоним: Firstname)
        /// </summary>
        [BarsUp.Utils.Display("Фамилия")]
        [BarsUp.Utils.Attributes.Uid("a5bb6593-a22c-4987-8fb0-41168fa0c432")]
        public virtual System.String Firstname
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Имя' (псевдоним: Name)
        /// </summary>
        [BarsUp.Utils.Display("Имя")]
        [BarsUp.Utils.Attributes.Uid("74d93d84-1667-49bc-bfda-2773baed8273")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Отчество' (псевдоним: Secondname)
        /// </summary>
        [BarsUp.Utils.Display("Отчество")]
        [BarsUp.Utils.Attributes.Uid("b5102beb-b78c-4483-9ee3-7f1a5f85b20f")]
        public virtual System.String Secondname
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [BarsUp.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [BarsUp.Utils.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}