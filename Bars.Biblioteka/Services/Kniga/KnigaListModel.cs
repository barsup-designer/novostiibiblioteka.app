namespace Bars.Biblioteka
{
    using System.Diagnostics;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Книга'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Книга")]
    public class KnigaListModel : DataTransferObject
    {
        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [BarsUp.Utils.Display("Идентификатор типа сущности")]
        [BarsUp.Utils.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [BarsUp.Utils.Display("Наименование редактора сущности")]
        [BarsUp.Utils.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Название' (псевдоним: name)
        /// </summary>
        [BarsUp.Utils.Display("Название")]
        [BarsUp.Utils.Attributes.Uid("39ee6428-7732-4c6c-a7b0-be3b3c397169")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Год издания' (псевдоним: Data)
        /// </summary>
        [BarsUp.Utils.Display("Год издания")]
        [BarsUp.Utils.Attributes.Uid("74092c9f-f687-4785-903c-dc9686cd0a77")]
        public virtual System.Int32? Data
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [BarsUp.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [BarsUp.Utils.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}