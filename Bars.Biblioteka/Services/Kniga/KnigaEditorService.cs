namespace Bars.Biblioteka
{
    using System.Threading.Tasks;
    using System.Diagnostics;
    using BarsUp.Core.Exceptions;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Filter;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.Fias;
    using BarsUp.Modules.FileStorage;
    using BarsUp.Modules.Filter;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils.Annotations;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт сервиса редактора 'Форма редактирования Книга'
    /// </summary>
    public interface IKnigaEditorService : IEditorViewService<Bars.Biblioteka.Kniga, KnigaEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Книга'
    /// </summary>
    public interface IKnigaEditorServiceHandler : IEditorViewServiceHandler<Bars.Biblioteka.Kniga, KnigaEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Книга'
    /// </summary>
    public abstract class AbstractKnigaEditorServiceHandler : EditorViewServiceHandler<Bars.Biblioteka.Kniga, KnigaEditorModel>, IKnigaEditorServiceHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Книга'
    /// </summary>
    public class KnigaEditorService : EditorViewService<Bars.Biblioteka.Kniga, KnigaEditorModel>, IKnigaEditorService
    {
        public KnigaEditorService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override async Task<KnigaEditorModel> CreateModelInternalAsync(BaseParams @params)
        {
            await Task.CompletedTask;
            var model = new KnigaEditorModel();
            var varPublishingId = @params.JsonData.GetAs<long? >("Publishing_Id");
            if (varPublishingId != null)
            {
                var queryPublishing = Container.ResolveDomain<BarsUp.Demo.LibraryEntities.Components.Publisher.Entities.Publisher>().GetAll().Where(x => x.Id == varPublishingId);
                model.Publishing = queryPublishing.Select(x => new KnigaEditorPublishingControlModel{Id = x.Id, Name = x.Name, }).FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Книга' в модель представления
        /// </summary>
        protected override async Task<KnigaEditorModel> MapEntityInternalAsync(Bars.Biblioteka.Kniga entity)
        {
            await Task.CompletedTask;
            // создаем экзепляр модели
            var model = new KnigaEditorModel();
            model.Id = entity.Id;
            model.name = ((System.String)(entity.name));
            model.Data = ((System.Int32? )(entity.Data));
            model.Publishing = entity.Publishing == null ? null : new KnigaEditorPublishingControlModel{Id = (System.Int64? )(entity.Return(p => p.Publishing).Return(p => p.Id)), Name = entity.Return(p => p.Publishing).Return(p => p.Name), };
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Книга' из модели представления		
        /// </summary>
        protected override async Task UnmapEntityInternalAsync(Bars.Biblioteka.Kniga entity, KnigaEditorModel model, IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            await Task.CompletedTask;
            entity.name = model.name;
            entity.Data = model.Data;
            entity.Publishing = isNested ? await TryGetEntityByIdAsync<BarsUp.Demo.LibraryEntities.Components.Publisher.Entities.Publisher>(model.Publishing?.Id) : await TryReadEntityByIdAsync<BarsUp.Demo.LibraryEntities.Components.Publisher.Entities.Publisher>(model.Publishing?.Id);
        }
    }
}