namespace Bars.Biblioteka
{
    using Castle.Windsor;
    using System.Diagnostics;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Attributes;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Lists;
    using BarsUp.Designer.GeneratedApp.Queries;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System;
    using NHibernate.Linq;
    using BarsUp.Designer.GeneratedApp;
    using System.Threading.Tasks;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Книга'
    /// </summary>
    public interface IKnigaListService : IListViewService<Bars.Biblioteka.Kniga, Bars.Biblioteka.KnigaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра для сервиса представления 'Реестр Книга'
    /// </summary>
    public interface IKnigaListServiceFilter : IListViewServiceFilter<Bars.Biblioteka.Kniga, Bars.Biblioteka.KnigaListModel, BaseParams>
    {
    }

    /// <summary>
    /// Сервис для представления 'Реестр Книга'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Книга")]
    public class KnigaListService : ListViewService<Bars.Biblioteka.Kniga, Bars.Biblioteka.KnigaListModel>, IKnigaListService
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        public KnigaListService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Biblioteka.Kniga> Filter(IQueryable<Bars.Biblioteka.Kniga> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Biblioteka.KnigaListModel> Map(IQueryable<Bars.Biblioteka.Kniga> entityQuery)
        {
            var query = DataStore.ReadAll<Bars.Biblioteka.Kniga>();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Biblioteka.KnigaListModel{Id = x.Id, _TypeUid = "89c9948b-99d0-499c-ae5f-a9e16f919547", name = ((System.String)(x.name)), Data = ((System.Int32? )(x.Data)), });
        }

        protected override async Task ProcessResultAsync(IList<Bars.Biblioteka.KnigaListModel> elements, BaseParams @params)
        {
            await Task.CompletedTask;
        }
    }
}