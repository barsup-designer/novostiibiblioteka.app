namespace Bars.Biblioteka
{
    using BarsUp.Windsor;
    using Newtonsoft.Json;
    using BarsUp.Core.Exceptions;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using BarsUp.Designer.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Книга'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Форма редактирования Книга")]
    public class KnigaEditorModel : DataTransferObject
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public KnigaEditorModel()
        {
        }

        /// <summary>
        /// Свойство 'Название'
        /// </summary>
        [BarsUp.Utils.Display("Название")]
        [BarsUp.Utils.Attributes.Uid("3de84dcf-870c-4651-8f8b-8bf270fde67a")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Год издания'
        /// </summary>
        [BarsUp.Utils.Display("Год издания")]
        [BarsUp.Utils.Attributes.Uid("d0fc882a-ffb4-4c98-91c1-090af6945f8f")]
        public virtual System.Int32? Data
        {
            get;
            set;
        }

        /// <summary>
        /// Издательство
        /// </summary>
        [BarsUp.Utils.Display("Издательство")]
        [BarsUp.Utils.Attributes.Uid("bac1fe16-02d0-476b-86bb-b1d83778c086")]
        public virtual Bars.Biblioteka.KnigaEditorPublishingControlModel Publishing
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Издательство'
    /// </summary>
    public class KnigaEditorPublishingControlModel
    {
        /// <summary>
        /// Свойство 'Книга.Издательство.Идентификатор'
        /// </summary>
        [BarsUp.Utils.DisplayAttribute("Книга.Издательство.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Книга.Издательство.Название'
        /// </summary>
        [BarsUp.Utils.DisplayAttribute("Книга.Издательство.Название")]
        public System.String Name
        {
            get;
            set;
        }
    }
}