namespace Bars.Biblioteka
{
    using BarsUp.Windsor;
    using Newtonsoft.Json;
    using BarsUp.Core.Exceptions;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using BarsUp.Designer.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Издательство'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Форма редактирования Издательство")]
    public class IzdatelStvoEditorModel : DataTransferObject
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public IzdatelStvoEditorModel()
        {
        }

        /// <summary>
        /// Свойство 'Название'
        /// </summary>
        [BarsUp.Utils.Display("Название")]
        [BarsUp.Utils.Attributes.Uid("cc44cd1c-855e-4a0e-b5f7-63859af8ca84")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Книги
        /// </summary>
        [BarsUp.Utils.Display("Книги")]
        [BarsUp.Utils.Attributes.Uid("fa673c17-40d3-40bb-b3c9-0c3d10e06ecf")]
        public virtual Bars.Biblioteka.KnigaListModel Books
        {
            get;
            set;
        }
    }
}