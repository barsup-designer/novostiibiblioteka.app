namespace Bars.Biblioteka
{
    using System.Diagnostics;
    using BarsUp.Core.Serialization;
    using BarsUp.DataAccess;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.IoC;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Издательство'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Издательство")]
    public class IzdatelStvoListModel : DataTransferObject
    {
        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [BarsUp.Utils.Display("Идентификатор типа сущности")]
        [BarsUp.Utils.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [BarsUp.Utils.Display("Наименование редактора сущности")]
        [BarsUp.Utils.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Название' (псевдоним: Name)
        /// </summary>
        [BarsUp.Utils.Display("Название")]
        [BarsUp.Utils.Attributes.Uid("9e40eae5-21b3-4fde-a363-b4ca58f8af88")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [BarsUp.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [BarsUp.Utils.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}