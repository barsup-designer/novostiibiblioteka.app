namespace Bars.Biblioteka
{
    using System.Threading.Tasks;
    using System.Diagnostics;
    using BarsUp.Core.Exceptions;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.Filter;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Editors;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.Fias;
    using BarsUp.Modules.FileStorage;
    using BarsUp.Modules.Filter;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils.Annotations;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт сервиса редактора 'Форма редактирования Издательство'
    /// </summary>
    public interface IIzdatelStvoEditorService : IEditorViewService<Bars.Biblioteka.IzdatelStvo, IzdatelStvoEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Издательство'
    /// </summary>
    public interface IIzdatelStvoEditorServiceHandler : IEditorViewServiceHandler<Bars.Biblioteka.IzdatelStvo, IzdatelStvoEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Издательство'
    /// </summary>
    public abstract class AbstractIzdatelStvoEditorServiceHandler : EditorViewServiceHandler<Bars.Biblioteka.IzdatelStvo, IzdatelStvoEditorModel>, IIzdatelStvoEditorServiceHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Издательство'
    /// </summary>
    public class IzdatelStvoEditorService : EditorViewService<Bars.Biblioteka.IzdatelStvo, IzdatelStvoEditorModel>, IIzdatelStvoEditorService
    {
        public IzdatelStvoEditorService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override async Task<IzdatelStvoEditorModel> CreateModelInternalAsync(BaseParams @params)
        {
            await Task.CompletedTask;
            var model = new IzdatelStvoEditorModel();
            var varBooksId = @params.JsonData.GetAs<long? >("Books_Id");
            if (varBooksId != null)
            {
                var _KnigaListService = Container.Resolve<Bars.Biblioteka.IKnigaListService>();
                model.Books = await _KnigaListService.GetByIdAsync(varBooksId.Value);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Издательство' в модель представления
        /// </summary>
        protected override async Task<IzdatelStvoEditorModel> MapEntityInternalAsync(Bars.Biblioteka.IzdatelStvo entity)
        {
            await Task.CompletedTask;
            // создаем экзепляр модели
            var model = new IzdatelStvoEditorModel();
            model.Id = entity.Id;
            model.Name = ((System.String)(entity.Name));
            if (entity.Books != null)
            {
                var serviceKnigaList = Container.Resolve<Bars.Biblioteka.IKnigaListService>();
                model.Books = await serviceKnigaList.GetByIdAsync(entity.Books.Id);
                Container.Release(serviceKnigaList);
                serviceKnigaList = null;
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Издательство' из модели представления		
        /// </summary>
        protected override async Task UnmapEntityInternalAsync(Bars.Biblioteka.IzdatelStvo entity, IzdatelStvoEditorModel model, IDictionary<string, FileData> requestFiles, IList<BarsUp.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            await Task.CompletedTask;
            entity.Name = model.Name;
            entity.Books = isNested ? await TryGetEntityByIdAsync<Bars.Biblioteka.Kniga>(model.Books?.Id) : await TryReadEntityByIdAsync<Bars.Biblioteka.Kniga>(model.Books?.Id);
        }
    }
}