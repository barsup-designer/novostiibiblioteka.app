namespace Bars.Biblioteka
{
    using Castle.Windsor;
    using System.Diagnostics;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.Attributes;
    using BarsUp.Designer.GeneratedApp.Commons;
    using BarsUp.Designer.GeneratedApp.Lists;
    using BarsUp.Designer.GeneratedApp.Queries;
    using BarsUp.Extensions.Json;
    using BarsUp.IoC;
    using BarsUp.Modules.PostgreSql.DataAccess;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using BarsUp;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System;
    using NHibernate.Linq;
    using BarsUp.Designer.GeneratedApp;
    using System.Threading.Tasks;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Издательство'
    /// </summary>
    public interface IIzdatelStvoListService : IListViewService<Bars.Biblioteka.IzdatelStvo, Bars.Biblioteka.IzdatelStvoListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра для сервиса представления 'Реестр Издательство'
    /// </summary>
    public interface IIzdatelStvoListServiceFilter : IListViewServiceFilter<Bars.Biblioteka.IzdatelStvo, Bars.Biblioteka.IzdatelStvoListModel, BaseParams>
    {
    }

    /// <summary>
    /// Сервис для представления 'Реестр Издательство'
    /// </summary>
    [BarsUp.Utils.DisplayAttribute("Реестр Издательство")]
    [ApiAccessible("Реестр Издательство")]
    public class IzdatelStvoListService : ListViewService<Bars.Biblioteka.IzdatelStvo, Bars.Biblioteka.IzdatelStvoListModel>, IIzdatelStvoListService
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        public IzdatelStvoListService(IWindsorContainer container): base(container)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.Biblioteka.IzdatelStvo> Filter(IQueryable<Bars.Biblioteka.IzdatelStvo> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.Biblioteka.IzdatelStvoListModel> Map(IQueryable<Bars.Biblioteka.IzdatelStvo> entityQuery)
        {
            var query = DataStore.ReadAll<Bars.Biblioteka.IzdatelStvo>();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.Biblioteka.IzdatelStvoListModel{Id = x.Id, _TypeUid = "99afd345-6a21-4f78-abc1-f571e92780d4", Name = ((System.String)(x.Name)), });
        }

        protected override async Task ProcessResultAsync(IList<Bars.Biblioteka.IzdatelStvoListModel> elements, BaseParams @params)
        {
            // устанавливаем контроллер редактора для каждой из сущностей
            foreach (var itm in elements)
            {
                switch (itm._TypeUid)
                {
                    case "99afd345-6a21-4f78-abc1-f571e92780d4":
                        itm._EditorName = "IzdatelStvoEditor";
                        break;
                }
            }

            await Task.CompletedTask;
        }
    }
}