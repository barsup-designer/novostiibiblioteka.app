// идентификатор модуля для миграций
[assembly: BarsUp.Ecm7.Framework.MigrationModule("Bars.Biblioteka")]
// идентификатор модуля
[assembly: System.Runtime.InteropServices.Guid("10165c9e-592c-4fa7-85ec-2d2ab980c5e5")]
namespace Bars.Biblioteka
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.IoC;
    using BarsUp.Registrar;
    using BarsUp.ResourceBundling;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [BarsUp.Utils.Display("Библиотека")]
    [BarsUp.Utils.Description("")]
    [BarsUp.Utils.CustomValue("Version", "2019.0214.174")]
    [BarsUp.Utils.Attributes.Uid("10165c9e-592c-4fa7-85ec-2d2ab980c5e5")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.Biblioteka.ResourceManifest>();
            Container.RegisterSingleton<BarsUp.Designer.GeneratedApp.Reports.ReportContract.IReportContractProvider, ReportContractProvider>();
            Container.RegisterTransient<BarsUp.License.ILicenseInfo, LicenceInformation>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
        }

        protected override void SetDependencies()
        {
            base.SetDependencies();
            DependsOn<BarsUp.Designer.GeneratedApp.Module>();
        }
    }
}