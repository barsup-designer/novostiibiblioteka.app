namespace Bars.Biblioteka
{
    using BarsUp.DataAccess;
    using BarsUp;
    using System.Linq.Expressions;
    using System.Linq;
    using Castle.Windsor;
    using System;

    public class ModuleDependencies : BaseModuleDependencies
    {
        public ModuleDependencies(IWindsorContainer container): base(container)
        {
            RegisterKnigaDependencies();
        }

#region Книга и его наследники
        private void RegisterKnigaDependencies()
        {
#region Книга
            References.Add(new EntityReference{ReferenceName = "Издательство", BaseEntity = typeof(Bars.Biblioteka.Kniga), CheckAnyDependences = id => Any<Bars.Biblioteka.IzdatelStvo>(x => x.Books.Id == id), GetCountDependences = id => Count<Bars.Biblioteka.IzdatelStvo>(x => x.Books.Id == id), GetDescriptionDependences = null});
#endregion
        }

#endregion
        private IQueryable<T> GetAll<T>() => Container.Resolve<IDataStore>().GetAll<T>();
        private bool Any<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Any(predicate);
        private int Count<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Count(predicate);
        private void SetNull<T>(Expression<Func<T, bool>> predicate, Action<T> action)
            where T : IEntity
        {
            var ds = Container.Resolve<IDataStore>();
            var records = ds.GetAll<T>().Where(predicate).ToArray();
            foreach (var record in records)
            {
                action.Invoke(record);
                ds.Update(record);
            }
        }

        private void RemoveRefs<T>(Expression<Func<T, bool>> predicate)
            where T : IEntity
        {
            var domainService = Container.ResolveDomain<T>();
            var entities = domainService.GetAll().Where(predicate).Select(x => new
            {
            Type = x.GetType(), Id = x.Id
            }

            ).ToArray().GroupBy(x => x.Type);
            foreach (var type in entities)
            {
                var x = type.Key;
                if (typeof(NHibernate.Proxy.INHibernateProxy).IsAssignableFrom(x))
                    x = type.Key.BaseType;
                var ds = Container.Resolve(typeof(IDomainService<>).MakeGenericType(x)) as IDomainService;
                foreach (var v in type)
                    ds.Delete(v.Id);
            }
        }
    }
}