namespace Bars.Biblioteka
{
    using BarsUp.Application;
    using BarsUp.DataAccess;
    using BarsUp.Modules.NHibernateChangeLog;
    using BarsUp;
    using System;
    using BarsUp.Utils;
    using System.Linq;
    using BarsUp;
    using Castle.Windsor;

    /// <summary>
    /// Провайдер логируемых сущностей
    /// </summary>
    public class AuditLogMapProvider : IAuditLogMapProvider
    {
        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name = "container">Контейнер реализаций логируемых сущностей</param>
        public void Init(IAuditLogMapContainer container)
        {
        }
    }
}