namespace Bars.Biblioteka
{
    using BarsUp.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Издательство
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Издательство")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("99afd345-6a21-4f78-abc1-f571e92780d4")]
    [BarsUp.Utils.Attributes.Uid("99afd345-6a21-4f78-abc1-f571e92780d4")]
    public class IzdatelStvo : BarsUp.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public IzdatelStvo(): base()
        {
        }

        /// <summary>
        /// Название
        /// </summary>
        [BarsUp.Utils.Display("Название")]
        [BarsUp.Utils.Attributes.Uid("8276fe2f-d1c4-4bbf-914d-12276246825f")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Книги
        /// </summary>
        [BarsUp.Utils.Display("Книги")]
        [BarsUp.Utils.Attributes.Uid("9eba5ff3-66ec-4c89-9398-cfb5654bbe92")]
        public virtual Bars.Biblioteka.Kniga Books
        {
            get;
            set;
        }
    }
}