namespace Bars.Biblioteka
{
    using BarsUp.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Книга
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Книга")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("89c9948b-99d0-499c-ae5f-a9e16f919547")]
    [BarsUp.Utils.Attributes.Uid("89c9948b-99d0-499c-ae5f-a9e16f919547")]
    public class Kniga : BarsUp.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Kniga(): base()
        {
        }

        /// <summary>
        /// Название
        /// </summary>
        [BarsUp.Utils.Display("Название")]
        [BarsUp.Utils.Attributes.Uid("c7349302-a1f7-4137-b0d8-bdf1b6184f12")]
        public virtual System.String name
        {
            get;
            set;
        }

        /// <summary>
        /// Год издания
        /// </summary>
        [BarsUp.Utils.Display("Год издания")]
        [BarsUp.Utils.Attributes.Uid("ea3085ed-3de4-4145-8923-73477911d734")]
        public virtual System.Int32? Data
        {
            get;
            set;
        }

        /// <summary>
        /// Издательство
        /// </summary>
        [BarsUp.Utils.Display("Издательство")]
        [BarsUp.Utils.Attributes.Uid("27b78d99-5a38-4d83-8374-7fd8e31206f5")]
        public virtual BarsUp.Demo.LibraryEntities.Components.Publisher.Entities.Publisher Publishing
        {
            get;
            set;
        }
    }
}