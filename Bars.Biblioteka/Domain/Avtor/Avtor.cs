namespace Bars.Biblioteka
{
    using BarsUp.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Автор
    /// 
    /// </summary>
    [BarsUp.Utils.Display(@"Автор")]
    [BarsUp.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("2143cebd-96b8-4103-9ed0-56afd273400e")]
    [BarsUp.Utils.Attributes.Uid("2143cebd-96b8-4103-9ed0-56afd273400e")]
    public class Avtor : BarsUp.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Avtor(): base()
        {
        }

        /// <summary>
        /// Фамилия
        /// </summary>
        [BarsUp.Utils.Display("Фамилия")]
        [BarsUp.Utils.Attributes.Uid("a0a1d1b2-40be-4a2f-97a0-c74d7f9acef3")]
        public virtual System.String Firstname
        {
            get;
            set;
        }

        /// <summary>
        /// Имя
        /// </summary>
        [BarsUp.Utils.Display("Имя")]
        [BarsUp.Utils.Attributes.Uid("f63d8102-de9d-4983-9ee0-2adeb7cfcb20")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Отчество
        /// </summary>
        [BarsUp.Utils.Display("Отчество")]
        [BarsUp.Utils.Attributes.Uid("d64ee74c-9cc2-48eb-8f75-9007ac3a196e")]
        public virtual System.String Secondname
        {
            get;
            set;
        }
    }
}