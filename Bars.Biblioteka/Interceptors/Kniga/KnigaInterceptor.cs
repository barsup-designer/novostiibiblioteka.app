namespace Bars.Biblioteka
{
    using BarsUp.Modules.Scripting.Interfaces;
    using BarsUp.Modules.Scripting.Services;
    using System;
    using BarsUp;

    internal class KnigaDomainServiceInterceptor : EmptyDomainInterceptor<Kniga>
    {
        private readonly IScriptExecutor _scriptExecutor;
        public KnigaDomainServiceInterceptor(IScriptExecutor scriptExecutor)
        {
            _scriptExecutor = scriptExecutor;
        }

        private void RunScript(Kniga entity, string scriptResourceName, string eventDisplayName)
        {
            var scriptContext = new BaseScriptContext(Container);
            scriptContext.SetValue("entity", entity);
            try
            {
                _scriptExecutor.RunFromResource(scriptContext, scriptResourceName, typeof(Kniga).Assembly);
            }
            catch (Exception exc)
            {
                throw new ApplicationException($"Не удалось выполнить скрипт события сущности\"{eventDisplayName}\":<br>" + exc.Message);
            }
        }
    }
}