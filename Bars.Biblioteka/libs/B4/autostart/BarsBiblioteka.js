Ext.define('B4.autostart.BarsBiblioteka', {
    requires: [
        'BarsUp.flexdesk.viewport.Default'],
    singleton: true,
    onBeforeCreate: {
        autostartAfter: ['B4.autostart.FlexDeskAutostart'],
        fn: function(cfg) {
            cfg.mainView = 'BarsUp.flexdesk.viewport.Default';
        }
    }
});