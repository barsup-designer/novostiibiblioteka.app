Ext.define('B4.controller.AvtorList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.AvtorList',
        'B4.model.AvtorListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-avtorlist',
    viewUid: 'e7121741-7046-4e43-bcf0-c9ce70fbcd88',
    viewDataName: 'AvtorList',
    // набор описаний действий реестра
    actions: {},
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-avtorlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
    },
});