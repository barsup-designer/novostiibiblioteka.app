Ext.define('B4.controller.KnigaList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.KnigaList',
        'B4.model.KnigaListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-knigalist',
    viewUid: '320ef596-90b8-403d-b0a4-7141a991e6bf',
    viewDataName: 'KnigaList',
    // набор описаний действий реестра
    actions: {},
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-knigalist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
    },
});