Ext.define('B4.controller.IzdatelStvoList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.IzdatelStvoList',
        'B4.model.IzdatelStvoListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-izdatelstvolist',
    viewUid: '8e4dbc2f-2ea1-4535-804c-f5e84e410d1c',
    viewDataName: 'IzdatelStvoList',
    // набор описаний действий реестра
    actions: {
        'Addition-IzdatelStvoEditor-InWindow': {
            'editor': 'IzdatelStvoEditor',
            'editorAlias': 'rms-izdatelstvoeditor',
            'editorUid': '807113cf-aefc-4ddb-be69-99979012cdb8',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-IzdatelStvoEditor-InWindow': {
            'editor': 'IzdatelStvoEditor',
            'editorAlias': 'rms-izdatelstvoeditor',
            'editorUid': '807113cf-aefc-4ddb-be69-99979012cdb8',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-izdatelstvolist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    // Не открываем редактирование, если спрятан верхний тулбар
                    if (sender.panel.dockedItems && !sender.panel.dockedItems.items.filter(x => x.dock == 'top' && x.xtype == 'toolbar').length) return;
                    this.defaultEditActionHandler(false, 'Editing-IzdatelStvoEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-izdatelstvolist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
    },
});