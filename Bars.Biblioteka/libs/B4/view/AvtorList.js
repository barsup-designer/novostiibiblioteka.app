Ext.define('B4.view.AvtorList', {
    'alias': 'widget.rms-avtorlist',
    'dataSourceUid': '2143cebd-96b8-4103-9ed0-56afd273400e',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.AvtorListModel',
    'stateful': false,
    'title': 'Реестр Автор',
    requires: [
        'B4.model.AvtorListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'BarsUp.filter.ExtGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '881ee6e0-4a3e-40b3-a6cc-d8662124167c',
        'text': 'Обновить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Firstname',
                'dataIndexAbsoluteUid': 'FieldPath://2143cebd-96b8-4103-9ed0-56afd273400e$a0a1d1b2-40be-4a2f-97a0-c74d7f9acef3',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a5bb6593-a22c-4987-8fb0-41168fa0c432',
                'sortable': true,
                'text': 'Фамилия',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Name',
                'dataIndexAbsoluteUid': 'FieldPath://2143cebd-96b8-4103-9ed0-56afd273400e$f63d8102-de9d-4983-9ee0-2adeb7cfcb20',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '74d93d84-1667-49bc-bfda-2773baed8273',
                'sortable': true,
                'text': 'Имя',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Secondname',
                'dataIndexAbsoluteUid': 'FieldPath://2143cebd-96b8-4103-9ed0-56afd273400e$d64ee74c-9cc2-48eb-8f75-9007ac3a196e',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b5102beb-b78c-4483-9ee3-7f1a5f85b20f',
                'sortable': true,
                'text': 'Отчество',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'gridfilters',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});