Ext.define('B4.view.IzdatelStvoEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-izdatelstvoeditor',
    title: 'Форма редактирования Издательство',
    rmsUid: '807113cf-aefc-4ddb-be69-99979012cdb8',
    requires: [
        'B4.form.PickerField',
        'B4.model.KnigaListModel',
        'B4.view.KnigaList',
        'BarsUp.versioning.plugin.PickerField'],
    statics: {},
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'IzdatelStvo_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'IzdatelStvo_Name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Books',
        'modelProperty': 'IzdatelStvo_Books',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '807113cf-aefc-4ddb-be69-99979012cdb8-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'IzdatelStvoEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Название',
                    'labelAlign': 'top',
                    'modelProperty': 'IzdatelStvo_Name',
                    'name': 'Name',
                    'rmsUid': 'cc44cd1c-855e-4a0e-b5f7-63859af8ca84',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'f16ea0ba-c2bb-4556-b3ea-c0020029c4d0',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '9144a69c-1023-4f17-aad7-d4faca69e30d',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Книги',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.KnigaList',
                    'listViewCtl': 'B4.controller.KnigaList',
                    'model': 'B4.model.KnigaListModel',
                    'modelProperty': 'IzdatelStvo_Books',
                    'name': 'Books',
                    'plugins': [{
                        'ptype': 'versioningpickerfieldplugin'
                    }],
                    'rmsUid': 'fa673c17-40d3-40bb-b3c9-0c3d10e06ecf',
                    'textProperty': 'name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Книги',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '0f0713a8-47ba-4cc8-8229-b1b851449c98',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'd94646e9-b987-4607-8a3c-83b34767b3a6',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'IzdatelStvo_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});