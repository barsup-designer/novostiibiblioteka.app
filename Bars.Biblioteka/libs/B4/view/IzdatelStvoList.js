Ext.define('B4.view.IzdatelStvoList', {
    'alias': 'widget.rms-izdatelstvolist',
    'dataSourceUid': '99afd345-6a21-4f78-abc1-f571e92780d4',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.IzdatelStvoListModel',
    'stateful': false,
    'title': 'Реестр Издательство',
    requires: [
        'B4.model.IzdatelStvoListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'BarsUp.filter.ExtGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'f71dfb91-cf94-4d1b-a2df-e6f0dcc5bb8e',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-IzdatelStvoEditor-InWindow',
        'rmsUid': 'c3b07710-cd01-4982-b03f-8f7b98ab3fa0',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-IzdatelStvoEditor-InWindow',
        'rmsUid': '95681078-c96c-465b-829d-21c9547d9da9',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '35c0414a-01c3-40d8-9aca-c5ef59ef00c9',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Name',
                'dataIndexAbsoluteUid': 'FieldPath://99afd345-6a21-4f78-abc1-f571e92780d4$8276fe2f-d1c4-4bbf-914d-12276246825f',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '9e40eae5-21b3-4fde-a363-b4ca58f8af88',
                'sortable': true,
                'text': 'Название',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'gridfilters',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});