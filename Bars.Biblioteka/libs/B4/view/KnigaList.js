Ext.define('B4.view.KnigaList', {
    'alias': 'widget.rms-knigalist',
    'dataSourceUid': '89c9948b-99d0-499c-ae5f-a9e16f919547',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.KnigaListModel',
    'stateful': false,
    'title': 'Реестр Книга',
    requires: [
        'B4.model.KnigaListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'BarsUp.filter.ExtGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '95a04a95-73ff-4994-927e-d3b649fa2582',
        'text': 'Обновить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'name',
                'dataIndexAbsoluteUid': 'FieldPath://89c9948b-99d0-499c-ae5f-a9e16f919547$c7349302-a1f7-4137-b0d8-bdf1b6184f12',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '39ee6428-7732-4c6c-a7b0-be3b3c397169',
                'sortable': true,
                'text': 'Название',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Data',
                'dataIndexAbsoluteUid': 'FieldPath://89c9948b-99d0-499c-ae5f-a9e16f919547$ea3085ed-3de4-4145-8923-73477911d734',
                'filter': {
                    'xtype': 'b4-filter-field-list-of-number-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '74092c9f-f687-4785-903c-dc9686cd0a77',
                'sortable': true,
                'text': 'Год издания',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'gridfilters',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});