Ext.define('B4.view.AvtorEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-avtoreditor',
    title: 'Форма редактирования Автор',
    rmsUid: '56975756-c993-4b72-8051-dbed47852d22',
    requires: [],
    statics: {},
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Avtor_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Firstname',
        'modelProperty': 'Avtor_Firstname',
        'type': 'TextField'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'Avtor_Name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Secondname',
        'modelProperty': 'Avtor_Secondname',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '56975756-c993-4b72-8051-dbed47852d22-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'AvtorEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Фамилия',
                    'labelAlign': 'top',
                    'modelProperty': 'Avtor_Firstname',
                    'name': 'Firstname',
                    'rmsUid': 'dbe56ed8-e5d9-4726-817d-d7ce70cf9c34',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '7834da05-32f3-4976-8aa1-98ddb6f24616',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'ce51460a-0e32-492b-b6d0-82e1d7210fa5',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Имя',
                    'labelAlign': 'top',
                    'modelProperty': 'Avtor_Name',
                    'name': 'Name',
                    'rmsUid': 'd1755db2-fe7c-4d3d-9f6b-512385b49708',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '67669e1f-0bcd-4fff-92bb-5d2c5b5de645',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '4206af40-3ebb-42b8-8f38-236cca2e81a8',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Отчество',
                    'labelAlign': 'top',
                    'modelProperty': 'Avtor_Secondname',
                    'name': 'Secondname',
                    'rmsUid': 'a271bbab-a0f3-49b0-adfc-8c507c5b99dc',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '9e3ee189-3b2e-433b-8a55-ede6dcabb107',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'a9ba6927-ecba-4ece-a33b-23174f8eb695',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Avtor_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});