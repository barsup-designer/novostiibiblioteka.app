Ext.define('B4.view.KnigaEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-knigaeditor',
    title: 'Форма редактирования Книга',
    rmsUid: '5306625c-c678-41c5-8fae-c0c4f1d52b93',
    requires: [
        'B4.form.PickerField',
        'BarsUp.versioning.plugin.PickerField'],
    statics: {},
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Kniga_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'name',
        'modelProperty': 'Kniga_name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Data',
        'modelProperty': 'Kniga_Data',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Publishing',
        'modelProperty': 'Kniga_Publishing',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '5306625c-c678-41c5-8fae-c0c4f1d52b93-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'KnigaEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'anchor': '100%',
                    'fieldLabel': 'Название',
                    'labelAlign': 'top',
                    'modelProperty': 'Kniga_name',
                    'name': 'name',
                    'rmsUid': '3de84dcf-870c-4651-8f8b-8bf270fde67a',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '7e38929a-d9ce-4ca6-8af1-58df958c70ed',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'a72bddef-04d1-4cd8-90a8-aa6124c73f88',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowDecimals': false,
                    'anchor': '100%',
                    'fieldLabel': 'Год издания',
                    'labelAlign': 'top',
                    'modelProperty': 'Kniga_Data',
                    'name': 'Data',
                    'rmsUid': 'd0fc882a-ffb4-4c98-91c1-090af6945f8f',
                    'xtype': 'numberfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'e6b63f9c-214d-4a1d-bb97-cfd97b84347e',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'f1dd2758-98b1-4853-90be-a5a8547cb370',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [],
                'layout': 'anchor',
                'rmsUid': 'ba9363ac-9262-431e-a60c-ff70f21f6f0b',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '85ea9cfa-baf6-4993-8108-fbbf63e0bc7d',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Kniga_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('name'));
        } else {}
        return res;
    },
});