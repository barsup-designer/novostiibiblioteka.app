// идентификатор модуля для миграций
[assembly: BarsUp.Ecm7.Framework.MigrationModule("Bars.ModulNews.Custom")]
// идентификатор модуля
[assembly: System.Runtime.InteropServices.Guid("e7b55d81-63ae-fb60-1a1b-7c7bf08049fd")]
namespace Bars.ModulNews.Custom
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.IoC;
    using BarsUp.Registrar;
    using BarsUp.ResourceBundling;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [BarsUp.Utils.Display("Новости.Разработка")]
    [BarsUp.Utils.Description("Кастомная часть для модуля ModulNews")]
    [BarsUp.Utils.CustomValue("Version", "2018.1203.27")]
    [BarsUp.Utils.Attributes.Uid("e7b55d81-63ae-fb60-1a1b-7c7bf08049fd")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.ModulNews.Custom.ResourceManifest>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
        }

        protected override void SetDependencies()
        {
            base.SetDependencies();
            DependsOn<Bars.ModulNews.Module>();
        }
    }
}