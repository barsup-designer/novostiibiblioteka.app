namespace BarsUp.WebHost
{
    using BarsUp.Web;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore;
    using System.Text;
    using System;

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.GetEncoding(65001);
            WebHost.CreateDefaultBuilder().UseBarsUpDefaults(args).UseKestrel().UseIISIntegration().UseStartup<BarsUpMvcStartup>().Build().Run();
        }
    }
}