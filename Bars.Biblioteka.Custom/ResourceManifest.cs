namespace Bars.Biblioteka.Custom
{
    using BarsUp.Utils;
    using BarsUp;

    /// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public partial class ResourceManifest
    {
        /// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name = "container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {
        }

        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");
            container.Add(webPath, "Bars.Biblioteka.Custom.dll/Bars.Biblioteka.Custom.{0}".FormatUsing(resourceName));
        }
    }
}