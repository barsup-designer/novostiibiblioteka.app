namespace Bars.Biblioteka.Custom
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp.States;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.IoC;
    using BarsUp.Modules.Versioning.Extensions;
    using BarsUp.Modules.Versioning.Interfaces;
    using BarsUp.Modules.Versioning;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    public partial class Module
    {
        protected virtual void RegisterDomainServices()
        {
        }
    }
}