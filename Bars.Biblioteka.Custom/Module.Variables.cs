namespace Bars.Biblioteka.Custom
{
    using BarsUp;
    using BarsUp.Abstractions.Variables;
    using BarsUp.Application;
    using BarsUp.DataAccess;
    using BarsUp.Designer.Core.TypeSystem;
    using BarsUp.Designer.Core;
    using BarsUp.Designer.GeneratedApp;
    using BarsUp.IoC;
    using BarsUp.Modules.Variables;
    using BarsUp.Utils;
    using BarsUp.Windsor;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    /// <summary>
    /// Класс модуля. Регистрация переменных
    /// </summary>    
    public partial class Module
    {
        protected virtual void RegisterVariables()
        {
        }
    }
}