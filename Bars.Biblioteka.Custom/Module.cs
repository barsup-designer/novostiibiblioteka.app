// идентификатор модуля для миграций
[assembly: BarsUp.Ecm7.Framework.MigrationModule("Bars.Biblioteka.Custom")]
// идентификатор модуля
[assembly: System.Runtime.InteropServices.Guid("6e407934-7b45-45e3-ea7f-b962f3c30c8d")]
namespace Bars.Biblioteka.Custom
{
    using BarsUp.Windsor;
    using BarsUp.DataAccess;
    using BarsUp.IoC;
    using BarsUp.Registrar;
    using BarsUp.ResourceBundling;
    using BarsUp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [BarsUp.Utils.Display("Библиотека.Разработка")]
    [BarsUp.Utils.Description("Кастомная часть для модуля Biblioteka")]
    [BarsUp.Utils.CustomValue("Version", "2018.1203.27")]
    [BarsUp.Utils.Attributes.Uid("6e407934-7b45-45e3-ea7f-b962f3c30c8d")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.Biblioteka.Custom.ResourceManifest>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
        }

        protected override void SetDependencies()
        {
            base.SetDependencies();
            DependsOn<Bars.Biblioteka.Module>();
        }
    }
}